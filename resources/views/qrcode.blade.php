<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"> 
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"/>
</head>
<body>
    <div class="container mt-4">
        <!-- <div class="card">
            <div class="card-header">
                <h2>Simple QR Code</h2>
            </div>
            <div class="card-body">
                 {!! QrCode::size(300)->generate('RemoteStack') !!} 
            </div>
        </div> -->
        <div class="card">
            <div class="card-header">
                <h2>Color QR Code</h2>
            </div>
            <div class="card-body">
                <!-- {!! QrCode::size(300)->backgroundColor(255,90,0)->generate('https://rbh.melu.in/qrcode-with-image') !!} -->

               <!--  {!! QrCode::backgroundColor(255,55,0)
                         ->merge('uploads/settings/5d3a6c13-b4bd-4bec-914e-8c0a11d19a59_logo.jpg', 0.5, true)
                         ->size(200)->errorCorrection('H')->generate('https://rbh.melu.in/qrcode-with-image') !!} -->

<!-- {{
    QrCode::backgroundColor(255,55,0)
                         ->merge('uploads/settings/5d3a6c13-b4bd-4bec-914e-8c0a11d19a59_logo.jpg', 0.5, true)
                         ->size(200)->errorCorrection('H')
                         ->generate('webnersolutions.com')
}}     -->

<img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->backgroundColor(255,55,0)
                         ->merge('uploads/settings/5d3a6c13-b4bd-4bec-914e-8c0a11d19a59_logo.jpg', 0.5, true)
                         ->size(200)->errorCorrection('H')
                         ->generate('webnersolutions.com')) !!} ">  

<img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('uploads/settings/5d3a6c13-b4bd-4bec-914e-8c0a11d19a59_logo.jpg', 0.5, true)
                        ->size(200)->errorCorrection('H')
                        ->generate('https://rbh.melu.in/restaurant/ganeshfastfoodcenter')) !!} ">                     
            </div>
        </div>
    </div>
</body>
</html>