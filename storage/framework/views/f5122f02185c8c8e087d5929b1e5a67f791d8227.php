<?php echo strip_tags($header); ?>


<?php echo strip_tags($slot); ?>

<?php if(isset($subcopy)): ?>

<?php echo strip_tags($subcopy); ?>

<?php endif; ?>

<?php echo strip_tags($footer); ?>

<?php /**PATH /home/bd9i6ac6nk1z/public_html/rbh/vendor/laravel/framework/src/Illuminate/Mail/resources/views/text/layout.blade.php ENDPATH**/ ?>