<a href="/login"  class="btn btn-neutral btn-icon web-menu">
    <span class="btn-inner--icon">
      <i class="fa fa-user mr-2"></i>
    </span>
    <span class="nav-link-inner--text"><?php echo e(__('Login')); ?></span>
</a>
<a href="/login"  class="nav-link nav-link-icon mobile-menu">
    <span class="btn-inner--icon">
      <i class="fa fa-user mr-2"></i>
    </span>
    <span class="nav-link-inner--text"><?php echo e(__('Login')); ?></span>
</a>

<?php /**PATH /home/bd9i6ac6nk1z/public_html/rbh/resources/views/layouts/menu/partials/guest.blade.php ENDPATH**/ ?>