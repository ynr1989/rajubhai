<?php

namespace App\Http\Controllers;

use App\Restorant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Tables;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class QRController extends Controller
{
    public function index()
    {
        $domain = config('app.url');
        $linkToTheMenu = $domain.'/'.config('settings.url_route').'/'.auth()->user()->restorant->subdomain;

        if (config('settings.wildcard_domain_ready')) {
            $linkToTheMenu = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] ? 'https://' : 'http://').auth()->user()->restorant->subdomain.'.'.str_replace('www.', '', $_SERVER['HTTP_HOST']);
        }

        $dataToPass = [
            'url'=>$linkToTheMenu,
            'titleGenerator'=>__('Restaurant QR Generators'),
            'selectQRStyle'=>__('SELECT QR STYLE'),
            'selectQRColor'=>__('SELECT QR COLOR'),
            'color1'=>__('Color 1'),
            'color2'=>__('Color 2'),
            'titleDownload'=>__('QR Downloader'),
            'downloadJPG'=>__('Download JPG'),
            'titleTemplate'=>__('Menu Print template'),
            'downloadPrintTemplates'=>__('Download Print Templates'),
            'templates'=>explode(',', config('settings.templates')),
            'linkToTemplates'=>env('linkToTemplates', '/impactfront/img/templates.zip'),
        ];

        return view('qrsaas.qrgen')->with('data', json_encode($dataToPass));
    }

    public function generateQrCodeWithImg(){
      /*  $image = \QrCode::format('png')
                        ->backgroundColor(255,55,0)
                         ->merge('uploads/settings/5d3a6c13-b4bd-4bec-914e-8c0a11d19a59_logo.jpg', 0.5, true)
                         ->size(200)->errorCorrection('H')
                         ->generate('webnersolutions.com');
        return response($image)->header('Content-type','image/png');*/

        return view('qrcode');
    }


    public function downloadQRWithImg()
    { 

        if (config('settings.wildcard_domain_ready')) {
            $vendorURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] ? 'https://' : 'http://').auth()->user()->restorant->subdomain.'.'.str_replace('www.', '', $_SERVER['HTTP_HOST']);
        } else {
            $vendorURL = route('vendor', auth()->user()->restorant->subdomain);
        }

        $filename = auth()->user()->restorant->id.'_qr.png'; 
        $image = \QrCode::format('png')
                        //->backgroundColor(255,55,0)
                         ->merge(env('QRCODE_IMG_PATH'), 0.5, true)
                         ->size(200)->errorCorrection('H')
                         ->generate($vendorURL);  

        $output_file = "/img/qr-code/$filename";
        Storage::disk('public')->put($output_file, $image); //storage/app/public/img/qr-code/img-1557309130.png
        
        $url = env('APP_URL')."storage/img/qr-code/".$filename;
        $tempImage = tempnam(sys_get_temp_dir(), $filename);
        @copy($url, $tempImage);

        return response()->download($tempImage, $filename);
    }
}
