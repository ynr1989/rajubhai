-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 01, 2022 at 01:47 PM
-- Server version: 5.6.51-cll-lve
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `naga_ec1`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `apartment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `intercom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `floor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `address`, `created_at`, `updated_at`, `lat`, `lng`, `active`, `user_id`, `apartment`, `intercom`, `floor`, `entry`) VALUES
(1, '27759 Zulauf Gardens\nRoobborough, HI 78690-8301', '2021-12-27 22:40:30', '2021-12-27 22:40:30', '40.621997', '-73.938831', 1, 4, NULL, NULL, NULL, NULL),
(2, '375 Enrique Hill\nNew Carlieton, RI 34816-3325', '2021-12-27 22:40:30', '2021-12-27 22:40:30', '40.621997', '-73.938831', 1, 5, NULL, NULL, NULL, NULL),
(3, '47170 Rachel Port\nWest Sherwood, OR 45844-3885', '2021-12-27 22:40:30', '2021-12-27 22:40:30', '40.621997', '-73.938831', 1, 4, NULL, NULL, NULL, NULL),
(4, '36609 Reichert Greens\nNorth Kaylatown, NC 67005-6596', '2021-12-27 22:40:30', '2021-12-27 22:40:30', '40.621997', '-73.938831', 1, 4, NULL, NULL, NULL, NULL),
(5, '1796 Ethyl Trafficway\nPort Julianaville, AZ 57507', '2021-12-27 22:40:30', '2021-12-27 22:40:30', '40.621997', '-73.938831', 1, 4, NULL, NULL, NULL, NULL),
(6, '384 Fritsch Falls Apt. 541\nNew Marisol, OK 10012', '2021-12-27 22:40:30', '2021-12-27 22:40:30', '40.621997', '-73.938831', 1, 4, NULL, NULL, NULL, NULL),
(7, '300 Ledner Greens Apt. 399\nLake Zacharybury, NC 88314-7010', '2021-12-27 22:40:30', '2021-12-27 22:40:30', '40.621997', '-73.938831', 1, 5, NULL, NULL, NULL, NULL),
(8, '3619 Alexandrine Island Suite 203\nEberttown, MO 56228-7335', '2021-12-27 22:40:30', '2021-12-27 22:40:30', '40.621997', '-73.938831', 1, 4, NULL, NULL, NULL, NULL),
(9, '27310 Breitenberg Fields Suite 258\nDovieport, MA 38470', '2021-12-27 22:40:30', '2021-12-27 22:40:30', '40.621997', '-73.938831', 1, 5, NULL, NULL, NULL, NULL),
(10, '97943 Rosenbaum Mill Apt. 929\nVedatown, TX 47619', '2021-12-27 22:40:30', '2021-12-27 22:40:30', '40.621997', '-73.938831', 1, 4, NULL, NULL, NULL, NULL),
(11, '1, Gokul Degree College, Gopal Rao Pally, Prem Nagar, Erragadda, Hyderabad, Telangana, India', '2022-02-25 01:16:23', '2022-02-25 01:16:23', '17.4562667', '78.4307724', 1, 44, '123', NULL, '3rd Floor', '1'),
(12, '1, Gokul Degree College, Gopal Rao Pally, Prem Nagar, Erragadda, Hyderabad, Telangana, India', '2022-02-25 01:18:19', '2022-02-25 01:18:19', '17.4562667', '78.4307724', 1, 39, '12', '45', '3', NULL),
(13, '434, Erragadda, Hyderabad, Telangana, India', '2022-02-28 00:19:24', '2022-02-28 00:19:24', '17.455867', '78.4251967', 1, 47, '454', '12', '1', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '0 - Vendor, 1 - Blog',
  `vendor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `page_id` bigint(20) UNSIGNED DEFAULT NULL,
  `active_from` date NOT NULL,
  `active_to` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart_storage`
--

CREATE TABLE `cart_storage` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cart_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `vendor_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `type` int(11) NOT NULL,
  `receipt_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restorant_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order_index` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `restorant_id`, `created_at`, `updated_at`, `order_index`, `active`, `deleted_at`) VALUES
(72, '{\"en\":\"Tiffin\"}', 19, '2022-02-24 20:47:29', '2022-02-25 00:14:34', 1, 1, NULL),
(73, '{\"en\":\"Tiffin\"}', 19, '2022-02-24 20:47:38', '2022-02-24 21:53:57', 2, 1, '2022-02-24 21:53:57'),
(74, '{\"en\":\"Tiffin\"}', 19, '2022-02-24 20:47:45', '2022-02-24 21:54:06', 3, 1, '2022-02-24 21:54:06'),
(75, '{\"en\":\"Meals\"}', 19, '2022-02-24 20:48:04', '2022-02-25 00:14:34', 4, 1, NULL),
(76, '{\"en\":\"Biryani\"}', 19, '2022-02-24 20:48:20', '2022-02-24 20:48:20', 5, 1, NULL),
(77, '{\"te\":\"Snacks\"}', 20, '2022-02-27 03:33:24', '2022-02-27 03:33:24', 1, 1, NULL),
(78, '{\"te\":\"Soft Drinks\"}', 20, '2022-02-27 03:34:25', '2022-02-27 03:34:25', 2, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `header_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `header_subtitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `created_at`, `updated_at`, `name`, `alias`, `image`, `header_title`, `header_subtitle`, `deleted_at`) VALUES
(17, '2022-02-16 21:04:37', '2022-02-16 21:04:37', 'Hyderabad', 'hyd', '', 'Hyderabad', 'Hyderabad', NULL),
(18, '2022-02-24 17:14:07', '2022-02-24 17:14:07', 'Visakhapatnam', 'vsp', '', 'Visakhapatnam', 'vsp', NULL),
(19, '2022-02-24 17:14:38', '2022-02-24 17:14:38', 'Bangalore', 'blr', '', 'Bangalore', 'blr', NULL),
(20, '2022-02-24 17:15:14', '2022-02-24 17:15:14', 'Vizianagaram', 'vzm', '', 'Vizianagaram', 'vzm', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subdomain` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cover` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `minimum` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `fee` double(8,2) NOT NULL DEFAULT '0.00',
  `static_fee` double(8,2) NOT NULL DEFAULT '0.00',
  `radius` varchar(800) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `city_id` int(11) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `can_pickup` int(11) NOT NULL DEFAULT '1',
  `can_deliver` int(11) NOT NULL DEFAULT '1',
  `self_deliver` int(11) NOT NULL DEFAULT '0',
  `free_deliver` int(11) NOT NULL DEFAULT '0',
  `whatsapp_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb_username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `do_covertion` int(11) NOT NULL DEFAULT '1',
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_info` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mollie_payment_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `created_at`, `updated_at`, `name`, `subdomain`, `logo`, `cover`, `active`, `user_id`, `lat`, `lng`, `address`, `phone`, `minimum`, `description`, `fee`, `static_fee`, `radius`, `is_featured`, `city_id`, `views`, `can_pickup`, `can_deliver`, `self_deliver`, `free_deliver`, `whatsapp_phone`, `fb_username`, `do_covertion`, `currency`, `payment_info`, `mollie_payment_key`, `deleted_at`) VALUES
(19, '2022-02-24 16:46:59', '2022-03-02 04:53:24', 'Test Rajubhai', 'testrajubhai', '', '', 1, 44, '17.455867', '78.4251967', 'Erragadda, Hyderabad', '9036789012', '0', 'South Indian', 5.00, 0.00, '[{\"lat\":17.473879277833717,\"lng\":78.40991883745117},{\"lat\":17.479773818085047,\"lng\":78.43566804399414},{\"lat\":17.46814829178448,\"lng\":78.4552374409668},{\"lat\":17.452591849022557,\"lng\":78.46811204423828},{\"lat\":17.43687030517843,\"lng\":78.45626740922852},{\"lat\":17.42409555299225,\"lng\":78.4358397053711},{\"lat\":17.423440413392722,\"lng\":78.41609864702149},{\"lat\":17.437525396566677,\"lng\":78.40236573686524},{\"lat\":17.457013288934785,\"lng\":78.39395432939453},{\"lat\":17.469621991143264,\"lng\":78.39944749345703}]', 0, 17, 86, 1, 1, 0, 0, NULL, NULL, 0, '', '', '', NULL),
(20, '2022-02-27 03:20:17', '2022-03-02 04:53:06', 'KKP Rajubhai', 'kkprajubhai', '', '', 1, 46, '17.49217154043581', '78.41551193995818', 'Kukatpally', '+919949853126', '0', 'KKP Rajubhai', 0.00, 5.00, '[{\"lat\":17.53699009030896,\"lng\":78.39928993983611},{\"lat\":17.517347066077537,\"lng\":78.37457070155486},{\"lat\":17.49819307069191,\"lng\":78.37972054286345},{\"lat\":17.489188358050335,\"lng\":78.38675865931853},{\"lat\":17.481984266851683,\"lng\":78.3986032943283},{\"lat\":17.479691996180787,\"lng\":78.41662773890837},{\"lat\":17.485750077391515,\"lng\":78.42967400355681},{\"lat\":17.50163111601921,\"lng\":78.43825707240447},{\"lat\":17.522749109744577,\"lng\":78.44890007777556},{\"lat\":17.54026372086025,\"lng\":78.44323525233611},{\"lat\":17.533403947253365,\"lng\":78.42041015625},{\"lat\":17.540792873491615,\"lng\":78.40564141078337}]', 1, 17, 67, 1, 1, 0, 0, NULL, NULL, 0, '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE `configs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`id`, `value`, `key`, `model_type`, `model_id`, `created_at`, `updated_at`) VALUES
(1, '0', 'disable_callwaiter', 'App\\Restorant', 17, '2022-02-06 20:37:21', '2022-02-06 20:37:21'),
(2, '0', 'disable_ordering', 'App\\Restorant', 17, '2022-02-06 20:37:21', '2022-02-06 20:37:21'),
(3, '0', 'time_to_prepare_order_in_minutes', 'App\\Restorant', 17, '2022-02-06 20:42:06', '2022-02-06 20:42:06'),
(4, '30', 'delivery_interval_in_minutes', 'App\\Restorant', 17, '2022-02-06 20:42:06', '2022-02-06 20:42:06'),
(5, '0', 'disable_callwaiter', 'App\\Restorant', 18, '2022-02-20 23:29:05', '2022-02-20 23:29:05'),
(6, '0', 'disable_ordering', 'App\\Restorant', 18, '2022-02-20 23:29:05', '2022-02-20 23:29:05'),
(7, '0', 'disable_continues_ordering', 'App\\Restorant', 18, '2022-02-20 23:29:05', '2022-02-20 23:29:05'),
(8, '0', 'time_to_prepare_order_in_minutes', 'App\\Restorant', 18, '2022-02-20 23:29:55', '2022-02-20 23:29:55'),
(9, '30', 'delivery_interval_in_minutes', 'App\\Restorant', 18, '2022-02-20 23:29:55', '2022-02-20 23:29:55'),
(10, '0', 'disable_callwaiter', 'App\\Restorant', 19, '2022-02-24 16:46:59', '2022-02-24 16:46:59'),
(11, '0', 'disable_ordering', 'App\\Restorant', 19, '2022-02-24 16:46:59', '2022-02-24 16:46:59'),
(12, '0', 'disable_continues_ordering', 'App\\Restorant', 19, '2022-02-24 16:46:59', '2022-02-24 16:46:59'),
(13, '15', 'time_to_prepare_order_in_minutes', 'App\\Restorant', 19, '2022-02-24 16:53:21', '2022-02-24 16:53:21'),
(14, '5', 'delivery_interval_in_minutes', 'App\\Restorant', 19, '2022-02-24 16:53:21', '2022-02-24 16:53:21'),
(15, '0', 'disable_callwaiter', 'App\\Restorant', 20, '2022-02-27 03:20:17', '2022-02-27 03:20:17'),
(16, '0', 'disable_ordering', 'App\\Restorant', 20, '2022-02-27 03:20:17', '2022-02-27 03:20:17'),
(17, '0', 'disable_continues_ordering', 'App\\Restorant', 20, '2022-02-27 03:20:17', '2022-02-27 03:20:17'),
(18, '10', 'time_to_prepare_order_in_minutes', 'App\\Restorant', 20, '2022-02-27 03:25:02', '2022-02-27 03:25:02'),
(19, '30', 'delivery_interval_in_minutes', 'App\\Restorant', 20, '2022-02-27 03:25:02', '2022-02-27 03:25:02');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restaurant_id` bigint(20) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '0 - Fixed, 1 - Percentage',
  `price` double(8,2) NOT NULL,
  `active_from` date NOT NULL,
  `active_to` date NOT NULL,
  `limit_to_num_uses` int(11) NOT NULL,
  `used_count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `restaurant_id` bigint(20) UNSIGNED DEFAULT NULL,
  `expenses_category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `expenses_vendor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expenses_category`
--

CREATE TABLE `expenses_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restaurant_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expenses_category`
--

INSERT INTO `expenses_category` (`id`, `name`, `code`, `restaurant_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Suppliers', 'C1', 1, NULL, NULL, NULL),
(2, 'Utilities', 'C2', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `expenses_vendor`
--

CREATE TABLE `expenses_vendor` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restaurant_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `extras`
--

CREATE TABLE `extras` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `extra_for_all_variants` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hours`
--

CREATE TABLE `hours` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `0_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `0_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `1_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `1_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `2_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `2_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `3_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `3_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `4_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `4_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `5_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `5_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `6_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `6_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `restorant_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hours`
--

INSERT INTO `hours` (`id`, `created_at`, `updated_at`, `0_from`, `0_to`, `1_from`, `1_to`, `2_from`, `2_to`, `3_from`, `3_to`, `4_from`, `4_to`, `5_from`, `5_to`, `6_from`, `6_to`, `restorant_id`) VALUES
(20, '2022-02-24 16:46:59', '2022-03-02 04:52:17', '9:00 AM', '5:00 PM', '9:00 AM', '11:00 PM', '9:00 AM', '5:00 PM', '9:00 AM', '11:00 PM', '9:00 AM', '5:00 PM', '9:00 AM', '11:00 PM', '5:00 AM', '11:00 PM', 19),
(21, '2022-02-27 03:20:17', '2022-03-02 04:35:39', '9:00 AM', '11:00 PM', '9:00 AM', '11:00 PM', '9:00 AM', '5:00 PM', '9:00 AM', '5:00 PM', '9:00 AM', '5:00 PM', '6:00 AM', '11:00 PM', '6:00 AM', '11:00 PM', 20);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(455) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `available` int(11) NOT NULL DEFAULT '1',
  `has_variants` int(11) NOT NULL DEFAULT '0',
  `vat` double(8,2) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `enable_system_variants` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `description`, `image`, `price`, `category_id`, `created_at`, `updated_at`, `available`, `has_variants`, `vat`, `deleted_at`, `enable_system_variants`) VALUES
(344, '{\"en\":\"Sambar Idli\"}', '{\"en\":\"Sambar Idli with Peanut Chutney\"}', 'd375f9af-3219-4fb6-a34c-598ed38bf710', 50.00, 72, '2022-02-24 20:51:34', '2022-02-26 20:41:44', 1, 0, 5.00, NULL, 0),
(345, '{\"en\":\"Ghee Karam Idli\"}', '{\"en\":\"Ghee Karam Idli\"}', 'f54372b7-a493-4970-89b3-7979bf7b4e7d', 55.00, 72, '2022-02-24 21:53:36', '2022-02-24 21:53:36', 1, 0, 0.00, NULL, 0),
(346, '{\"en\":\"Sambar Rice\"}', '{\"en\":\"Sambar Rice\"}', '', 60.00, 75, '2022-02-24 21:55:00', '2022-02-24 21:55:00', 1, 0, 0.00, NULL, 0),
(347, '{\"en\":\"Veg Biryani\"}', '{\"en\":\"Veg Biryani\"}', '', 80.00, 76, '2022-02-24 22:25:31', '2022-02-24 22:25:31', 1, 0, 0.00, NULL, 0),
(348, '{\"te\":\"Samosa\"}', '{\"te\":\"Aloo Samosa\"}', '', 20.00, 77, '2022-02-27 03:33:45', '2022-02-27 03:36:03', 1, 0, 5.00, NULL, 0),
(349, '{\"te\":\"Bhelpuri\"}', '{\"te\":\"Bhelpuri\"}', '', 30.00, 77, '2022-02-27 03:34:08', '2022-02-27 03:36:27', 1, 0, 5.00, NULL, 0),
(350, '{\"te\":\"Badam Milk\"}', '{\"te\":\"Badam Milk\"}', '', 30.00, 78, '2022-02-27 03:34:57', '2022-02-27 03:36:50', 1, 0, 12.00, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `item_has_allergens`
--

CREATE TABLE `item_has_allergens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED DEFAULT NULL,
  `allergen_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `localmenus`
--

CREATE TABLE `localmenus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `restaurant_id` bigint(20) UNSIGNED NOT NULL,
  `language` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `languageName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default` int(11) NOT NULL DEFAULT '0' COMMENT '0 - No, 1 - Yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `localmenus`
--

INSERT INTO `localmenus` (`id`, `created_at`, `updated_at`, `restaurant_id`, `language`, `languageName`, `default`) VALUES
(1, '2022-02-24 17:30:33', '2022-02-24 17:30:33', 19, 'en', 'English', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_08_29_200844_create_languages_table', 1),
(4, '2018_08_29_205156_create_translations_table', 1),
(5, '2019_05_03_000001_create_customer_columns', 1),
(6, '2019_05_03_000002_create_subscriptions_table', 1),
(7, '2019_05_03_000003_create_subscription_items_table', 1),
(8, '2020_02_06_010033_create_permission_tables', 1),
(9, '2020_02_06_022212_create_restorants_table', 1),
(10, '2020_02_09_015116_create_status_table', 1),
(11, '2020_02_09_152551_create_categories_table', 1),
(12, '2020_02_09_152656_create_items_table', 1),
(13, '2020_02_11_052117_create_address_table', 1),
(14, '2020_02_11_054259_create_orders_table', 1),
(15, '2020_02_20_094727_create_settings_table', 1),
(16, '2020_03_25_134914_create_pages_table', 1),
(17, '2020_04_03_143518_update_settings_table', 1),
(18, '2020_04_10_202027_create_payments_table', 1),
(19, '2020_04_11_165819_update_table_orders', 1),
(20, '2020_04_22_105556_update_items_table', 1),
(21, '2020_04_23_212320_update_restorants_table', 1),
(22, '2020_04_23_212502_update_orders_table', 1),
(23, '2020_04_28_112519_update_address_table', 1),
(24, '2020_05_07_122727_create_hours_table', 1),
(25, '2020_05_09_012646_update_orders_add_delivery_table', 1),
(26, '2020_05_09_024507_add_options_to_settings_table', 1),
(27, '2020_05_20_232204_create_notifications_table', 1),
(28, '2020_06_03_134258_change_radius_to_delivery_area_restorants_table', 1),
(29, '2020_06_26_131803_create_sms_verifications_table', 1),
(30, '2020_07_12_182829_create_extras_table', 1),
(31, '2020_07_14_155509_create_plan_table', 1),
(32, '2020_07_23_183000_update_restorants_with_featured', 1),
(33, '2020_07_28_131511_update_users_sms_verification', 1),
(34, '2020_07_30_102916_change_json_to_string', 1),
(35, '2020_08_01_014851_create_variants', 1),
(36, '2020_08_14_003718_create_ratings_table', 1),
(37, '2020_08_18_035731_update_table_plans', 1),
(38, '2020_08_18_053012_update_user_add_plan', 1),
(39, '2020_09_02_131604_update_orders_time_to_prepare', 1),
(40, '2020_09_09_080747_create_cities_table', 1),
(41, '2020_09_28_131250_update_item_softdelete', 1),
(42, '2020_10_24_150254_create_tables_table', 1),
(43, '2020_10_25_021321_create_visits_table', 1),
(44, '2020_10_26_004421_update_orders_client_nullable', 1),
(45, '2020_10_26_104351_update_restorant_table', 1),
(46, '2020_10_26_190049_update_plan', 1),
(47, '2020_10_27_180123_create_stripe_payment', 1),
(48, '2020_11_01_190615_update_user_table', 1),
(49, '2020_11_05_081140_create_paths_table', 1),
(50, '2020_11_14_111220_create_phone_in_orders', 1),
(51, '2020_11_17_211252_update_logo_in_settings', 1),
(52, '2020_11_25_182453_create_paypal_payment', 1),
(53, '2020_11_25_225536_update_user_for_paypal_subsc', 1),
(54, '2020_11_27_102829_update_restaurants_for_delivery_pickup', 1),
(55, '2020_11_27_165901_create_coupons_table', 1),
(56, '2020_12_02_192213_update_for_whatsapp_order', 1),
(57, '2020_12_02_195333_update_for_mollie_payment', 1),
(58, '2020_12_07_142304_create_banners_table', 1),
(59, '2020_12_10_155335_wp_address', 1),
(60, '2020_12_14_195627_update_for_paystack_sub', 1),
(61, '2020_12_15_130511_update_paystack_verification', 1),
(62, '2020_12_27_155822_create_restaurant_multilanguage', 1),
(63, '2020_12_27_162902_update_restaurant_with_currency', 1),
(64, '2021_01_16_093708_update_restorant_with_pay_link', 1),
(65, '2021_01_22_142723_create_crud_for_whatsapp_landing', 1),
(66, '2021_02_16_065037_create_configs_table', 2),
(67, '2021_02_18_140330_allow_null_on_config_value', 2),
(68, '2021_02_22_135519_update_settinga_table', 2),
(69, '2021_02_26_113854_order_fee_update', 2),
(70, '2021_03_23_135952_update_config_table', 2),
(71, '2021_04_17_002457_update_restables', 2),
(72, '2021_04_17_231310_update_restablesagain', 2),
(73, '2021_04_22_184249_update_user_with_staff', 2),
(74, '2021_04_26_190410_create_cart_storage_table', 2),
(75, '2021_05_19_032734_update_order_with_employee_id', 2),
(76, '2021_05_19_124342_create_simple_delivery_areas', 2),
(77, '2021_05_28_070715_create_expenses', 2),
(78, '2021_07_01_084847_make_restaurant_soft_delete', 2),
(79, '2021_07_16_170627_make_user_soft_delete', 2),
(80, '2021_07_16_172430_make_user_email_not_unique', 2),
(81, '2021_07_21_153807_rename_to_company', 2),
(82, '2021_07_21_162114_make_pure_saas', 2),
(83, '2021_08_04_073843_add_md_to_orders', 2),
(84, '2021_09_03_131601_update_with_default_variants', 2),
(85, '2021_09_04_091444_cat_sd', 2),
(86, '2021_10_09_162446_update_order_with_coupons', 2),
(87, '2021_11_06_104921_create_item_has_allergens', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(2, 'App\\User', 2),
(5, 'App\\User', 3),
(5, 'App\\User', 4),
(5, 'App\\User', 5),
(5, 'App\\User', 6),
(5, 'App\\User', 7),
(5, 'App\\User', 8),
(5, 'App\\User', 9),
(5, 'App\\User', 10),
(5, 'App\\User', 11),
(5, 'App\\User', 12),
(5, 'App\\User', 13),
(5, 'App\\User', 14),
(5, 'App\\User', 15),
(5, 'App\\User', 16),
(5, 'App\\User', 17),
(5, 'App\\User', 18),
(5, 'App\\User', 19),
(5, 'App\\User', 20),
(5, 'App\\User', 21),
(5, 'App\\User', 22),
(5, 'App\\User', 23),
(5, 'App\\User', 24),
(5, 'App\\User', 25),
(5, 'App\\User', 26),
(5, 'App\\User', 27),
(5, 'App\\User', 28),
(5, 'App\\User', 29),
(5, 'App\\User', 30),
(5, 'App\\User', 31),
(5, 'App\\User', 32),
(5, 'App\\User', 33),
(5, 'App\\User', 34),
(3, 'App\\User', 35),
(4, 'App\\User', 36),
(4, 'App\\User', 37),
(3, 'App\\User', 38),
(4, 'App\\User', 39),
(2, 'App\\User', 40),
(3, 'App\\User', 41),
(4, 'App\\User', 42),
(2, 'App\\User', 43),
(2, 'App\\User', 44),
(4, 'App\\User', 45),
(2, 'App\\User', 46),
(4, 'App\\User', 47);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('00a9e238-9091-4ba1-9408-692df4430f9e', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"Your order is ready.\",\"body\":\"Your order is ready for pickup. We are expecting you.\"}', NULL, '2022-02-25 01:56:09', '2022-02-25 01:56:09'),
('075cd74d-6cda-47f3-b6d0-61b472aa8ad4', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-01 04:48:16', '2022-03-01 04:48:16'),
('095a257e-0994-416c-b89b-e239d933c97a', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-27 03:49:19', '2022-02-27 03:49:19'),
('0efa4dbd-8379-4e2c-8bf9-c9ff27be9c94', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-25 01:59:04', '2022-02-25 01:59:04'),
('105b67f1-4ba3-434f-a840-ac7e58dfa1de', 'App\\Notifications\\OrderNotification', 'App\\User', 45, '{\"title\":\"Your order has been accepted\",\"body\":\"order#13 We are now working on it!\"}', NULL, '2022-02-26 21:20:18', '2022-02-26 21:20:18'),
('12b10354-3d2a-46e6-8a17-eb8886e9efe9', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-02 04:40:57', '2022-03-02 04:40:57'),
('1d6bfdf2-d145-41f6-af49-be51043d4b22', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"Your order has been accepted\",\"body\":\"order#6 We are now working on it!\"}', NULL, '2022-02-25 01:55:59', '2022-02-25 01:55:59'),
('1defb441-ba75-4b9e-babb-3dcff41f1b6c', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-02 00:16:58', '2022-03-02 00:16:58'),
('1ea24d9b-1841-4131-b150-74adc2cf77e0', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-26 21:17:42', '2022-02-26 21:17:42'),
('2163b8da-15f7-413f-b500-2db42ec55286', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-28 00:20:38', '2022-02-28 00:20:38'),
('293e6c78-9d59-410c-a67b-448480201efa', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-02 04:44:03', '2022-03-02 04:44:03'),
('2b4e8b92-539a-4cc7-ac32-5e7103eca061', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-02 00:14:04', '2022-03-02 00:14:04'),
('2e1d9f39-efd3-4718-8620-a5f21937fa3e', 'App\\Notifications\\OrderNotification', 'App\\User', 39, '{\"title\":\"Your order has been accepted\",\"body\":\"order#18 We are now working on it!\"}', NULL, '2022-02-27 04:07:13', '2022-02-27 04:07:13'),
('36e47ec3-4aaf-4cae-b705-ba848bb4099a', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-02 04:44:06', '2022-03-02 04:44:06'),
('403e4cdb-91c9-458d-a94b-28c3ddf64316', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-26 19:12:34', '2022-02-26 19:12:34'),
('438c6436-6e82-4f12-bf6b-e8a323c8efd3', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-25 03:18:34', '2022-02-25 03:18:34'),
('549d74cc-6277-4a0b-a821-aca01f1e722c', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-25 01:13:59', '2022-02-25 01:13:59'),
('559d30b5-0fca-4984-aa65-849f30f23848', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-27 03:53:21', '2022-02-27 03:53:21'),
('5cd4f019-b916-4379-b56c-48218864de72', 'App\\Notifications\\OrderNotification', 'App\\User', 2, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-06 16:20:01', '2022-02-06 16:20:01'),
('6549de60-9efb-45e3-88a1-9c21570c1f22', 'App\\Notifications\\OrderNotification', 'App\\User', 2, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-06 04:47:55', '2022-02-06 04:47:55'),
('71a9d838-0a58-429f-9267-cde21bd2345a', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-24 23:39:41', '2022-02-24 23:39:41'),
('826df436-84a7-4674-9974-49e829c8b934', 'App\\Notifications\\OrderNotification', 'App\\User', 1, '{\"title\":\"Order rejected\",\"body\":\"Unfortunately your order is rejected. There where issues with the order and we need to reject it. Pls contact us for more info.\"}', NULL, '2022-02-06 04:55:03', '2022-02-06 04:55:03'),
('83d6aebb-2446-4875-ba45-53ba6503b4c6', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-01 04:56:35', '2022-03-01 04:56:35'),
('860cd3af-f3c7-4d62-bd3c-e185f87356a6', 'App\\Notifications\\OrderNotification', 'App\\User', 2, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-06 04:27:18', '2022-02-06 04:27:18'),
('8e5b7bc7-1f72-4668-bc15-d1d7ef85621c', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-27 03:42:37', '2022-02-27 03:42:37'),
('917fa4a5-c398-4dab-924d-8318bcb6cb4e', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-02 00:20:12', '2022-03-02 00:20:12'),
('a163e84e-938f-4f73-8121-7a3cd85c348a', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-01 04:56:16', '2022-03-01 04:56:16'),
('a1894ca8-3042-4550-8eb8-b9ed7b6b87d5', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-02 04:52:51', '2022-03-02 04:52:51'),
('a4d68ea3-ea59-465d-9807-e14e4af7fee1', 'App\\Notifications\\OrderNotification', 'App\\User', 39, '{\"title\":\"Your order is ready.\",\"body\":\"Your order is ready for pickup. We are expecting you.\"}', NULL, '2022-02-27 04:07:33', '2022-02-27 04:07:33'),
('a5ef6fc6-22b1-42b5-82e6-3df02e60f2e2', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-25 18:26:42', '2022-02-25 18:26:42'),
('a6c6d69c-7187-4f24-93f3-699f4c36115b', 'App\\Notifications\\OrderNotification', 'App\\User', 2, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-16 21:01:27', '2022-02-16 21:01:27'),
('b3fd8577-c510-4415-9cc2-41c073625951', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-02 00:53:22', '2022-03-02 00:53:22'),
('b6b33287-fbcf-4486-8897-0d3432ae4364', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-28 00:20:06', '2022-02-28 00:20:06'),
('bd8e2aa6-e42f-4be8-80c5-792796231e83', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-27 03:37:29', '2022-02-27 03:37:29'),
('cd8ea192-f4cc-4867-8312-8224574da702', 'App\\Notifications\\OrderNotification', 'App\\User', 2, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-16 20:23:49', '2022-02-16 20:23:49'),
('d5b22100-f815-49fe-bd95-38116ea59684', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"Your order has been accepted\",\"body\":\"order#11 We are now working on it!\"}', NULL, '2022-02-26 20:44:45', '2022-02-26 20:44:45'),
('d5b3cf73-ddd9-4877-9a4c-3093210fbf95', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-26 20:42:41', '2022-02-26 20:42:41'),
('d6dd5ba4-4dda-41e8-89b8-b322db37f9c7', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-26 20:47:02', '2022-02-26 20:47:02'),
('db5188f4-b3c1-41ed-b491-39ce12d9cac4', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-01 04:47:48', '2022-03-01 04:47:48'),
('dccc9062-d194-4380-b5a7-bfa6e98f937b', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-28 00:43:46', '2022-02-28 00:43:46'),
('dd3c332b-3415-4f71-8e3b-b8a0948c9cf4', 'App\\Notifications\\OrderNotification', 'App\\User', 39, '{\"title\":\"Your order has been accepted\",\"body\":\"order#12 We are now working on it!\"}', NULL, '2022-02-26 20:47:29', '2022-02-26 20:47:29'),
('f1426df3-e384-4c17-871e-3ceb04d2bead', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-27 03:51:29', '2022-02-27 03:51:29'),
('f258b730-ded1-42aa-9343-5fc9e0e3324a', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-02 04:42:24', '2022-03-02 04:42:24'),
('f271facf-4410-4b2b-823b-3933868eb7e0', 'App\\Notifications\\OrderNotification', 'App\\User', 2, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-16 20:10:26', '2022-02-16 20:10:26'),
('f6aa70b5-3677-4226-ac1b-b9ae46599519', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-28 00:25:17', '2022-02-28 00:25:17'),
('f6d2f78e-812b-4a00-9617-d71828c42abb', 'App\\Notifications\\OrderNotification', 'App\\User', 2, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-02-16 20:05:48', '2022-02-16 20:05:48'),
('f9d02c6e-7d3d-4c21-a573-b4c9ce62b5fd', 'App\\Notifications\\OrderNotification', 'App\\User', 44, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-02 00:06:30', '2022-03-02 00:06:30'),
('fc135e72-c754-4eb1-9ddf-1fd22d40e014', 'App\\Notifications\\OrderNotification', 'App\\User', 46, '{\"title\":\"There is new order\",\"body\":\"You have just received an order\"}', NULL, '2022-03-01 04:44:25', '2022-03-01 04:44:25');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `token_number` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `address_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED DEFAULT NULL,
  `restorant_id` bigint(20) UNSIGNED NOT NULL,
  `driver_id` bigint(20) UNSIGNED DEFAULT NULL,
  `delivery_price` double(8,2) NOT NULL,
  `order_price` double(8,2) NOT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `srtipe_payment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fee` double(8,2) NOT NULL DEFAULT '0.00',
  `fee_value` double NOT NULL DEFAULT '0',
  `static_fee` double(8,2) NOT NULL DEFAULT '0.00',
  `delivery_method` int(11) NOT NULL DEFAULT '1' COMMENT '1- Delivery, 2- Pickup',
  `delivery_pickup_interval` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `vatvalue` double(8,2) DEFAULT '0.00',
  `payment_processor_fee` double(8,2) NOT NULL DEFAULT '0.00',
  `time_to_prepare` int(11) DEFAULT NULL,
  `table_id` bigint(20) UNSIGNED DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `md` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `coupon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` double(8,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `token_number`, `created_at`, `updated_at`, `address_id`, `client_id`, `restorant_id`, `driver_id`, `delivery_price`, `order_price`, `payment_method`, `payment_status`, `comment`, `lat`, `lng`, `srtipe_payment_id`, `fee`, `fee_value`, `static_fee`, `delivery_method`, `delivery_pickup_interval`, `vatvalue`, `payment_processor_fee`, `time_to_prepare`, `table_id`, `phone`, `whatsapp_address`, `payment_link`, `employee_id`, `deleted_at`, `md`, `coupon`, `discount`) VALUES
(1, '1001', '2022-02-24 23:33:49', '2022-02-24 23:33:49', NULL, 44, 19, NULL, 0.00, 50.00, 'cod', 'unpaid', ' dd', NULL, NULL, NULL, 5.00, 2.5, 0.00, 2, '1010_1015', 0.00, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'c4ca4238a0b923820dcc509a6f75849b', NULL, 0.00),
(2, '1002', '2022-02-24 23:37:00', '2022-02-24 23:37:00', NULL, 44, 19, NULL, 0.00, 105.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 5.25, 0.00, 2, '1015_1020', 0.00, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'c81e728d9d4c2f636f067f89cc14862c', NULL, 0.00),
(3, '1002', '2022-02-24 23:39:41', '2022-02-24 23:56:40', NULL, 44, 19, NULL, 0.00, 50.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 2.5, 0.00, 2, '1015_1020', 0.00, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'eccbc87e4b5ce2fe28308fd9f2a7baf3', NULL, 0.00),
(4, NULL, '2022-02-25 01:12:15', '2022-02-25 01:12:15', NULL, 44, 19, NULL, 0.00, 50.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 2.5, 0.00, 2, '1110_1115', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'a87ff679a2f3e71d9181a67b7542122c', NULL, 0.00),
(5, NULL, '2022-02-25 01:12:55', '2022-02-25 01:12:55', NULL, 44, 19, NULL, 0.00, 50.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 2.5, 0.00, 2, '1110_1115', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'e4da3b7fbbce2345d7772b0674a318d5', NULL, 0.00),
(6, NULL, '2022-02-25 01:13:59', '2022-02-25 01:56:33', NULL, 44, 19, NULL, 0.00, 50.00, 'cod', 'paid', ' ', NULL, NULL, NULL, 5.00, 2.5, 0.00, 2, '1110_1115', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '1679091c5a880faf6fb5e6087eb1b2dc', NULL, 0.00),
(7, NULL, '2022-02-25 01:59:04', '2022-02-25 01:59:04', 12, 39, 19, NULL, 5.00, 50.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 2.5, 0.00, 1, '1155_1160', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '8f14e45fceea167a5a36dedd4bea2543', NULL, 0.00),
(8, NULL, '2022-02-25 03:18:34', '2022-02-25 03:18:34', NULL, 39, 19, NULL, 0.00, 80.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 4, 0.00, 2, '1235_1240', 0.00, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'c9f0f895fb98ab9159f51fd0297e236d', NULL, 0.00),
(9, NULL, '2022-02-25 18:26:42', '2022-02-25 18:26:42', NULL, 39, 19, NULL, 0.00, 50.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 2.5, 0.00, 2, '705_710', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '45c48cce2e2d7fbdea1afc51c7c6ad26', NULL, 0.00),
(10, NULL, '2022-02-26 19:12:33', '2022-02-26 19:12:33', NULL, 39, 19, NULL, 0.00, 55.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 2.75, 0.00, 2, '750_755', 0.00, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'd3d9446802a44259755d38e6d163e820', NULL, 0.00),
(11, '1001', '2022-02-26 20:42:41', '2022-02-26 20:42:41', NULL, 44, 19, NULL, 0.00, 50.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 2.5, 0.00, 2, '840_845', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '6512bd43d9caa6e02c990b0a82652dca', NULL, 0.00),
(12, '1002', '2022-02-26 20:47:02', '2022-02-26 20:47:02', NULL, 39, 19, NULL, 0.00, 50.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 2.5, 0.00, 2, '845_850', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'c20ad4d76fe97759aa27a0c99bff6710', NULL, 0.00),
(13, '1002', '2022-02-26 21:17:41', '2022-02-26 21:17:41', NULL, 45, 19, NULL, 0.00, 205.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 10.25, 0.00, 2, '875_880', 7.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'c51ce410c124a10e0db5e4b97fc2af39', NULL, 0.00),
(14, '1001', '2022-02-27 03:37:29', '2022-02-27 03:37:29', NULL, 39, 20, NULL, 0.00, 20.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1260_1290', 1.00, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'aab3238922bcc25a6f606eb525ffdc56', NULL, 0.00),
(15, '1002', '2022-02-27 03:42:37', '2022-02-27 03:42:37', NULL, 39, 19, NULL, 0.00, 50.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 2.5, 0.00, 2, '1260_1265', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '9bf31c7ff062936a96d3c8bd1f8f2ff3', NULL, 0.00),
(16, '1002', '2022-02-27 03:49:19', '2022-02-27 03:49:19', NULL, 39, 20, NULL, 0.00, 20.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1260_1290', 1.00, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'c74d97b01eae257e44aa9d5bade97baf', NULL, 0.00),
(17, '1002', '2022-02-27 03:51:29', '2022-02-27 03:51:29', NULL, 39, 20, NULL, 0.00, 30.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1290_1320', 1.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '70efdf2ec9b086079795c442636b55fb', NULL, 0.00),
(18, '1002', '2022-02-27 03:53:21', '2022-02-27 04:08:17', NULL, 39, 20, NULL, 0.00, 30.00, 'cod', 'paid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1290_1320', 3.60, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '6f4922f45568161a8cdf4ad2299f6d23', NULL, 0.00),
(19, '1001', '2022-02-28 00:20:05', '2022-02-28 00:20:05', NULL, 39, 19, NULL, 0.00, 250.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 12.5, 0.00, 2, '1055_1060', 12.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '1f0e3dad99908345f7439f8ffabdffc4', NULL, 0.00),
(20, '1002', '2022-02-28 00:20:38', '2022-02-28 00:20:38', 13, 47, 19, NULL, 5.00, 50.00, 'cod', 'unpaid', ' Fghgg', NULL, NULL, NULL, 5.00, 2.5, 0.00, 1, '1055_1060', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '98f13708210194c475687be6106a3b84', NULL, 0.00),
(21, '1002', '2022-02-28 00:25:17', '2022-02-28 00:25:17', NULL, 39, 19, NULL, 0.00, 50.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 2.5, 0.00, 2, '1065_1070', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '3c59dc048e8850243be8079a5c74d079', NULL, 0.00),
(22, '1002', '2022-02-28 00:43:45', '2022-02-28 00:43:45', 13, 47, 19, NULL, 5.00, 50.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 2.5, 0.00, 1, '1080_1085', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'b6d767d2f8ed5d21a44b0e5886680cb9', NULL, 0.00),
(23, '1001', '2022-03-01 04:44:24', '2022-03-01 04:44:24', NULL, 1, 20, NULL, 0.00, 60.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1320_1350', 3.00, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '37693cfc748049e45d87b8c7d8b9aacd', NULL, 0.00),
(24, '1002', '2022-03-01 04:47:48', '2022-03-01 04:47:48', NULL, 1, 20, NULL, 0.00, 20.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1320_1350', 1.00, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '1ff1de774005f8da13f42943881c655f', NULL, 0.00),
(25, '1002', '2022-03-01 04:48:15', '2022-03-01 04:48:15', NULL, 1, 20, NULL, 0.00, 30.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1320_1350', 3.60, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '8e296a067a37563370ded05f5a3bf3ec', NULL, 0.00),
(26, '1003', '2022-03-01 04:56:15', '2022-03-01 04:56:15', NULL, 1, 20, NULL, 0.00, 60.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1320_1350', 7.20, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '4e732ced3463d06de0ca9a15b6153677', NULL, 0.00),
(27, '1004', '2022-03-01 04:56:35', '2022-03-01 04:56:35', NULL, 1, 20, NULL, 0.00, 30.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1350_1380', 1.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '02e74f10e0327ad868d138f2b4fdd6f0', NULL, 0.00),
(28, '1001', '2022-03-02 00:06:29', '2022-03-02 00:06:29', NULL, 39, 19, NULL, 0.00, 50.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 2.5, 0.00, 2, '1045_1050', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '33e75ff09dd601bbe69f351039152189', NULL, 0.00),
(29, '1001', '2022-03-02 00:14:04', '2022-03-02 00:14:04', NULL, 39, 20, NULL, 0.00, 20.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1050_1080', 1.00, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '6ea9ab1baa0efb9e19094440c317e21b', NULL, 0.00),
(30, '1002', '2022-03-02 00:16:36', '2022-03-02 00:16:36', NULL, 39, 20, NULL, 0.00, 20.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1050_1080', 1.00, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '34173cb38f07f89ddbebc2ac9128303f', NULL, 0.00),
(31, '1002', '2022-03-02 00:20:12', '2022-03-02 00:20:12', NULL, 39, 19, NULL, 0.00, 50.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 2.5, 0.00, 2, '1055_1060', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'c16a5320fa475530d9583c34fd356ef5', NULL, 0.00),
(32, '1003', '2022-03-02 00:53:21', '2022-03-02 00:53:21', NULL, 45, 19, NULL, 0.00, 190.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 9.5, 0.00, 2, '1090_1095', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '6364d3f0f495b6ab9dcf8d3b5c6e0b01', NULL, 0.00),
(33, '1003', '2022-03-02 04:40:57', '2022-03-02 04:40:57', NULL, 39, 20, NULL, 0.00, 20.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1320_1350', 1.00, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '182be0c5cdcd5072bb1864cdee4d3d6e', NULL, 0.00),
(34, '1004', '2022-03-02 04:42:23', '2022-03-02 04:42:23', NULL, 39, 20, NULL, 0.00, 30.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1320_1350', 1.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'e369853df766fa44e1ed0ff613f563bd', NULL, 0.00),
(35, '1005', '2022-03-02 04:44:03', '2022-03-02 04:44:03', NULL, 42, 20, NULL, 0.00, 20.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1320_1350', 1.00, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '1c383cd30b7c298ab50293adfecb7b18', NULL, 0.00),
(36, '1006', '2022-03-02 04:44:05', '2022-03-02 04:44:05', NULL, 39, 20, NULL, 0.00, 30.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 0.00, 0, 5.00, 2, '1320_1350', 3.60, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, '19ca14e7ea6328a42e0eb13d585e4c22', NULL, 0.00),
(37, '1004', '2022-03-02 04:52:51', '2022-03-02 04:52:51', NULL, 39, 19, NULL, 0.00, 50.00, 'cod', 'unpaid', ' ', NULL, NULL, NULL, 5.00, 2.5, 0.00, 2, '1330_1335', 2.50, 0.00, NULL, NULL, NULL, NULL, '', NULL, NULL, 'a5bfc9e07964f8dddeb95fc584cd965d', NULL, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `order_has_items`
--

CREATE TABLE `order_has_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '1',
  `extras` varchar(800) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `vat` double(8,2) DEFAULT '0.00',
  `vatvalue` double(8,2) DEFAULT '0.00',
  `variant_price` double(8,2) DEFAULT NULL,
  `variant_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_has_items`
--

INSERT INTO `order_has_items` (`id`, `created_at`, `updated_at`, `order_id`, `item_id`, `qty`, `extras`, `vat`, `vatvalue`, `variant_price`, `variant_name`) VALUES
(1, NULL, NULL, 1, 344, 1, '[]', 0.00, 0.00, 50.00, ''),
(2, NULL, NULL, 2, 344, 1, '[]', 0.00, 0.00, 50.00, ''),
(3, NULL, NULL, 2, 345, 1, '[]', 0.00, 0.00, 55.00, ''),
(4, NULL, '2022-02-24 23:56:40', 3, 344, 1, '[]', 0.00, NULL, 50.00, ''),
(5, NULL, NULL, 4, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(6, NULL, NULL, 5, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(7, NULL, NULL, 6, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(8, NULL, NULL, 7, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(9, NULL, NULL, 8, 347, 1, '[]', 0.00, 0.00, 80.00, ''),
(10, NULL, NULL, 9, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(11, NULL, NULL, 10, 345, 1, '[]', 0.00, 0.00, 55.00, ''),
(12, NULL, NULL, 11, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(13, NULL, NULL, 12, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(14, NULL, NULL, 13, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(15, NULL, NULL, 13, 345, 1, '[]', 0.00, 0.00, 55.00, ''),
(16, NULL, NULL, 13, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(17, NULL, NULL, 13, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(18, NULL, NULL, 14, 348, 1, '[]', 5.00, 1.00, 20.00, ''),
(19, NULL, NULL, 15, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(20, NULL, NULL, 16, 348, 1, '[]', 5.00, 1.00, 20.00, ''),
(21, NULL, NULL, 17, 349, 1, '[]', 5.00, 1.50, 30.00, ''),
(22, NULL, NULL, 18, 350, 1, '[]', 12.00, 3.60, 30.00, ''),
(23, NULL, NULL, 19, 344, 5, '[]', 5.00, 12.50, 50.00, ''),
(24, NULL, NULL, 20, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(25, NULL, NULL, 21, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(26, NULL, NULL, 22, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(27, NULL, NULL, 23, 349, 1, '[]', 5.00, 1.50, 30.00, ''),
(28, NULL, NULL, 23, 349, 1, '[]', 5.00, 1.50, 30.00, ''),
(29, NULL, NULL, 24, 348, 1, '[]', 5.00, 1.00, 20.00, ''),
(30, NULL, NULL, 25, 350, 1, '[]', 12.00, 3.60, 30.00, ''),
(31, NULL, NULL, 26, 350, 1, '[]', 12.00, 3.60, 30.00, ''),
(32, NULL, NULL, 26, 350, 1, '[]', 12.00, 3.60, 30.00, ''),
(33, NULL, NULL, 27, 349, 1, '[]', 5.00, 1.50, 30.00, ''),
(34, NULL, NULL, 28, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(35, NULL, NULL, 29, 348, 1, '[]', 5.00, 1.00, 20.00, ''),
(36, NULL, NULL, 30, 348, 1, '[]', 5.00, 1.00, 20.00, ''),
(37, NULL, NULL, 31, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(38, NULL, NULL, 32, 344, 1, '[]', 5.00, 2.50, 50.00, ''),
(39, NULL, NULL, 32, 346, 1, '[]', 0.00, 0.00, 60.00, ''),
(40, NULL, NULL, 32, 347, 1, '[]', 0.00, 0.00, 80.00, ''),
(41, NULL, NULL, 33, 348, 1, '[]', 5.00, 1.00, 20.00, ''),
(42, NULL, NULL, 34, 349, 1, '[]', 5.00, 1.50, 30.00, ''),
(43, NULL, NULL, 35, 348, 1, '[]', 5.00, 1.00, 20.00, ''),
(44, NULL, NULL, 36, 350, 1, '[]', 12.00, 3.60, 30.00, ''),
(45, NULL, NULL, 37, 344, 1, '[]', 5.00, 2.50, 50.00, '');

-- --------------------------------------------------------

--
-- Table structure for table `order_has_status`
--

CREATE TABLE `order_has_status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `status_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `comment` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_has_status`
--

INSERT INTO `order_has_status` (`id`, `created_at`, `updated_at`, `order_id`, `status_id`, `user_id`, `comment`) VALUES
(1, '2022-02-24 23:33:49', NULL, 1, 1, 44, ''),
(2, '2022-02-24 23:33:49', NULL, 1, 2, 1, 'Automatically approved by admin'),
(3, '2022-02-24 23:37:00', NULL, 2, 1, 44, ''),
(4, '2022-02-24 23:37:00', NULL, 2, 2, 1, 'Automatically approved by admin'),
(5, '2022-02-24 23:39:41', NULL, 3, 1, 44, ''),
(6, '2022-02-24 23:39:41', NULL, 3, 2, 1, 'Automatically approved by admin'),
(7, '2022-02-25 01:12:15', NULL, 4, 1, 44, ''),
(8, '2022-02-25 01:12:15', NULL, 4, 2, 1, 'Automatically approved by admin'),
(9, '2022-02-25 01:12:55', NULL, 5, 1, 44, ''),
(10, '2022-02-25 01:12:55', NULL, 5, 2, 1, 'Automatically approved by admin'),
(11, '2022-02-25 01:13:59', NULL, 6, 1, 44, ''),
(12, '2022-02-25 01:13:59', NULL, 6, 2, 1, 'Automatically approved by admin'),
(13, '2022-02-25 01:55:59', NULL, 6, 3, 44, ''),
(14, '2022-02-25 01:56:09', NULL, 6, 5, 44, ''),
(15, '2022-02-25 01:56:33', NULL, 6, 7, 44, ''),
(16, '2022-02-25 01:59:04', NULL, 7, 1, 39, ''),
(17, '2022-02-25 01:59:04', NULL, 7, 2, 1, 'Automatically approved by admin'),
(18, '2022-02-25 03:18:34', NULL, 8, 1, 39, ''),
(19, '2022-02-25 03:18:34', NULL, 8, 2, 1, 'Automatically approved by admin'),
(20, '2022-02-25 18:26:42', NULL, 9, 1, 39, ''),
(21, '2022-02-25 18:26:42', NULL, 9, 2, 1, 'Automatically approved by admin'),
(22, '2022-02-26 19:12:33', NULL, 10, 1, 39, ''),
(23, '2022-02-26 19:12:33', NULL, 10, 2, 1, 'Automatically approved by admin'),
(24, '2022-02-26 20:42:41', NULL, 11, 1, 44, ''),
(25, '2022-02-26 20:42:41', NULL, 11, 2, 1, 'Automatically approved by admin'),
(26, '2022-02-26 20:44:45', NULL, 11, 3, 44, ''),
(27, '2022-02-26 20:47:02', NULL, 12, 1, 39, ''),
(28, '2022-02-26 20:47:02', NULL, 12, 2, 1, 'Automatically approved by admin'),
(29, '2022-02-26 20:47:29', NULL, 12, 3, 44, ''),
(30, '2022-02-26 21:17:41', NULL, 13, 1, 45, ''),
(31, '2022-02-26 21:17:41', NULL, 13, 2, 1, 'Automatically approved by admin'),
(32, '2022-02-26 21:20:18', NULL, 13, 3, 44, ''),
(33, '2022-02-27 03:37:29', NULL, 14, 1, 39, ''),
(34, '2022-02-27 03:37:29', NULL, 14, 2, 1, 'Automatically approved by admin'),
(35, '2022-02-27 03:42:37', NULL, 15, 1, 39, ''),
(36, '2022-02-27 03:42:37', NULL, 15, 2, 1, 'Automatically approved by admin'),
(37, '2022-02-27 03:49:19', NULL, 16, 1, 39, ''),
(38, '2022-02-27 03:49:19', NULL, 16, 2, 1, 'Automatically approved by admin'),
(39, '2022-02-27 03:51:29', NULL, 17, 1, 39, ''),
(40, '2022-02-27 03:51:29', NULL, 17, 2, 1, 'Automatically approved by admin'),
(41, '2022-02-27 03:53:21', NULL, 18, 1, 39, ''),
(42, '2022-02-27 03:53:21', NULL, 18, 2, 1, 'Automatically approved by admin'),
(43, '2022-02-27 04:07:13', NULL, 18, 3, 46, ''),
(44, '2022-02-27 04:07:33', NULL, 18, 5, 46, ''),
(45, '2022-02-27 04:08:17', NULL, 18, 7, 46, ''),
(46, '2022-02-28 00:20:05', NULL, 19, 1, 39, ''),
(47, '2022-02-28 00:20:05', NULL, 19, 2, 1, 'Automatically approved by admin'),
(48, '2022-02-28 00:20:38', NULL, 20, 1, 47, ''),
(49, '2022-02-28 00:20:38', NULL, 20, 2, 1, 'Automatically approved by admin'),
(50, '2022-02-28 00:25:17', NULL, 21, 1, 39, ''),
(51, '2022-02-28 00:25:17', NULL, 21, 2, 1, 'Automatically approved by admin'),
(52, '2022-02-28 00:43:45', NULL, 22, 1, 47, ''),
(53, '2022-02-28 00:43:45', NULL, 22, 2, 1, 'Automatically approved by admin'),
(54, '2022-03-01 04:44:24', NULL, 23, 1, 1, ''),
(55, '2022-03-01 04:44:24', NULL, 23, 2, 1, 'Automatically approved by admin'),
(56, '2022-03-01 04:47:48', NULL, 24, 1, 1, ''),
(57, '2022-03-01 04:47:48', NULL, 24, 2, 1, 'Automatically approved by admin'),
(58, '2022-03-01 04:48:15', NULL, 25, 1, 1, ''),
(59, '2022-03-01 04:48:15', NULL, 25, 2, 1, 'Automatically approved by admin'),
(60, '2022-03-01 04:56:15', NULL, 26, 1, 1, ''),
(61, '2022-03-01 04:56:15', NULL, 26, 2, 1, 'Automatically approved by admin'),
(62, '2022-03-01 04:56:35', NULL, 27, 1, 1, ''),
(63, '2022-03-01 04:56:35', NULL, 27, 2, 1, 'Automatically approved by admin'),
(64, '2022-03-02 00:06:29', NULL, 28, 1, 39, ''),
(65, '2022-03-02 00:06:29', NULL, 28, 2, 1, 'Automatically approved by admin'),
(66, '2022-03-02 00:14:04', NULL, 29, 1, 39, ''),
(67, '2022-03-02 00:14:04', NULL, 29, 2, 1, 'Automatically approved by admin'),
(68, '2022-03-02 00:16:36', NULL, 30, 1, 39, ''),
(69, '2022-03-02 00:16:36', NULL, 30, 2, 1, 'Automatically approved by admin'),
(70, '2022-03-02 00:20:12', NULL, 31, 1, 39, ''),
(71, '2022-03-02 00:20:12', NULL, 31, 2, 1, 'Automatically approved by admin'),
(72, '2022-03-02 00:53:21', NULL, 32, 1, 45, ''),
(73, '2022-03-02 00:53:21', NULL, 32, 2, 1, 'Automatically approved by admin'),
(74, '2022-03-02 04:40:57', NULL, 33, 1, 39, ''),
(75, '2022-03-02 04:40:57', NULL, 33, 2, 1, 'Automatically approved by admin'),
(76, '2022-03-02 04:42:23', NULL, 34, 1, 39, ''),
(77, '2022-03-02 04:42:23', NULL, 34, 2, 1, 'Automatically approved by admin'),
(78, '2022-03-02 04:44:03', NULL, 35, 1, 42, ''),
(79, '2022-03-02 04:44:03', NULL, 35, 2, 1, 'Automatically approved by admin'),
(80, '2022-03-02 04:44:05', NULL, 36, 1, 39, ''),
(81, '2022-03-02 04:44:05', NULL, 36, 2, 1, 'Automatically approved by admin'),
(82, '2022-03-02 04:52:51', NULL, 37, 1, 39, ''),
(83, '2022-03-02 04:52:51', NULL, 37, 2, 1, 'Automatically approved by admin');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `showAsLink` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `created_at`, `updated_at`, `title`, `content`, `showAsLink`) VALUES
(1, '2021-12-27 22:40:27', '2022-02-06 22:44:03', 'Terms and conditions', '<p>This website is operated by&nbsp;<strong>CIPHERTEXT TECHNOLOGIES PRIVATE LIMITED</strong>. Throughout the site, the terms &ldquo;we&rdquo;, &ldquo;us&rdquo; and &ldquo;our&rdquo; refer to<strong>&nbsp;CIPHERTEXT TECHNOLOGIES PRIVATE LIMITED. CIPHERTEXT TECHNOLOGIES PRIVATE LIMITED</strong>&nbsp;offers this website, including all information, tools and services available from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here.</p>\r\n\r\n<p>By visiting our site and/ or purchasing something from us, you engage in our &ldquo;Service&rdquo; and agree to be bound by the following terms and conditions (&ldquo;Terms of Service&rdquo;, &ldquo;Terms&rdquo;), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms of Service apply to all users of the site, including without limitation users who are browsers, vendors, customers, merchants, and/ or contributors of content.</p>\r\n\r\n<p>Please read these Terms of Service carefully before accessing or using our website. By accessing or using any part of the site, you agree to be bound by these Terms of Service. If you do not agree to all the terms and conditions of this agreement, then you may not access the website or use any services. If these Terms of Service are considered an offer, acceptance is expressly limited to these Terms of Service.</p>\r\n\r\n<p>Any new features or tools which are added to the current store shall also be subject to the Terms of Service. You can review the most current version of the Terms of Service at any time on this page. We reserve the right to update, change or replace any part of these Terms of Service by posting updates and/or changes to our website. It is your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.</p>\r\n\r\n<p><strong>SECTION 1 &ndash; ONLINE STORE TERMS</strong></p>\r\n\r\n<p>By agreeing to these Terms of Service, you represent that you are at least the age of majority in your state or province of residence, or that you are the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site.</p>\r\n\r\n<p>You may not use our products for any illegal or unauthorized purpose nor may you, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright laws).</p>\r\n\r\n<p>You must not transmit any worms or viruses or any code of a destructive nature.</p>\r\n\r\n<p>A breach or violation of any of the Terms will result in an immediate termination of your Services.</p>\r\n\r\n<p><strong>SECTION 2 &ndash; GENERAL CONDITIONS</strong></p>\r\n\r\n<p>We reserve the right to refuse service to anyone for any reason at any time.</p>\r\n\r\n<p>You understand that your content (not including credit card information), may be transferred unencrypted and involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices. Credit card information is always encrypted during transfer over networks.</p>\r\n\r\n<p>You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service or any contact on the website through which the service is provided, without express written permission by us.</p>\r\n\r\n<p>The headings used in this agreement are included for convenience only and will not limit or otherwise affect these Terms.</p>\r\n\r\n<p><strong>SECTION 3 &ndash; ACCURACY, COMPLETENESS AND TIMELINESS OF INFORMATION</strong></p>\r\n\r\n<p>We are not responsible if information made available on this site is not accurate, complete or current. The material on this site is provided for general information only and should not be relied upon or used as the sole basis for making decisions without consulting primary, more accurate, more complete or more timely sources of information. Any reliance on the material on this site is at your own risk.</p>\r\n\r\n<p>This site may contain certain historical information. Historical information, necessarily, is not current and is provided for your reference only. We reserve the right to modify the contents of this site at any time, but we have no obligation to update any information on our site. You agree that it is your responsibility to monitor changes to our site.</p>\r\n\r\n<p><strong>SECTION 4 &ndash; MODIFICATIONS TO THE SERVICE AND PRICES</strong></p>\r\n\r\n<p>Prices for our products are subject to change without notice.</p>\r\n\r\n<p>We reserve the right at any time to modify or discontinue the Service (or any part or content thereof) without notice at any time.</p>\r\n\r\n<p>We shall not be liable to you or to any third-party for any modification, price change, suspension or discontinuance of the Service.</p>\r\n\r\n<p><strong>SECTION 5 &ndash; PRODUCTS OR SERVICES</strong></p>\r\n\r\n<p>Certain products or services may be available exclusively online through the website. These products or services may have limited quantities and are subject to return or exchange only according to our Return Policy.</p>\r\n\r\n<p>We have made every effort to display as accurately as possible the colors and images of our products that appear at the store. We cannot guarantee that your computer monitor&rsquo;s display of any color will be accurate.</p>\r\n\r\n<p>We reserve the right, but are not obligated, to limit the sales of our products or Services to any person, geographic region or jurisdiction. We may exercise this right on a case-by-case basis. We reserve the right to limit the quantities of any products or services that we offer. All descriptions of products or product pricing are subject to change at anytime without notice, at the sole discretion of us. We reserve the right to discontinue any product at any time. Any offer for any product or service made on this site is void where prohibited.</p>\r\n\r\n<p>We do not warrant that the quality of any products, services, information, or other material purchased or obtained by you will meet your expectations, or that any errors in the Service will be corrected.</p>\r\n\r\n<p><strong>SECTION 6 &ndash; ACCURACY OF BILLING AND ACCOUNT INFORMATION</strong></p>\r\n\r\n<p>We reserve the right to refuse any order you place with us. We may, in our sole discretion, limit or cancel quantities purchased per person, per household or per order. These restrictions may include orders placed by or under the same customer account, the same credit card, and/or orders that use the same billing and/or shipping address. In the event that we make a change to or cancel an order, we may attempt to notify you by contacting the e-mail and/or billing address/phone number provided at the time the order was made. We reserve the right to limit or prohibit orders that, in our sole judgment, appear to be placed by dealers, resellers or distributors.</p>\r\n\r\n<p>You agree to provide current, complete and accurate purchase and account information for all purchases made at our store. You agree to promptly update your account and other information, including your email address and credit card numbers and expiration dates, so that we can complete your transactions and contact you as needed.</p>\r\n\r\n<p>For more detail, please review our Returns Policy.</p>\r\n\r\n<p><strong>SECTION 7 &ndash; OPTIONAL TOOLS</strong></p>\r\n\r\n<p>We may provide you with access to third-party tools over which we neither monitor nor have any control nor input.</p>\r\n\r\n<p>You acknowledge and agree that we provide access to such tools &rdquo;as is&rdquo; and &ldquo;as available&rdquo; without any warranties, representations or conditions of any kind and without any endorsement. We shall have no liability whatsoever arising from or relating to your use of optional third-party tools.</p>\r\n\r\n<p>Any use by you of optional tools offered through the site is entirely at your own risk and discretion and you should ensure that you are familiar with and approve of the terms on which tools are provided by the relevant third-party provider(s).</p>\r\n\r\n<p>We may also, in the future, offer new services and/or features through the website (including, the release of new tools and resources). Such new features and/or services shall also be subject to these Terms of Service.</p>\r\n\r\n<p><strong>SECTION 8 &ndash; THIRD-PARTY LINKS</strong></p>\r\n\r\n<p>Certain content, products and services available via our Service may include materials from third-parties.</p>\r\n\r\n<p>Third-party links on this site may direct you to third-party websites that are not affiliated with us. We are not responsible for examining or evaluating the content or accuracy and we do not warrant and will not have any liability or responsibility for any third-party materials or websites, or for any other materials, products, or services of third-parties.</p>\r\n\r\n<p>We are not liable for any harm or damages related to the purchase or use of goods, services, resources, content, or any other transactions made in connection with any third-party websites. Please review carefully the third-party&rsquo;s policies and practices and make sure you understand them before you engage in any transaction. Complaints, claims, concerns, or questions regarding third-party products should be directed to the third-party.</p>\r\n\r\n<p><strong>SECTION 9 &ndash; USER COMMENTS, FEEDBACK AND OTHER SUBMISSIONS</strong></p>\r\n\r\n<p>If, at our request, you send certain specific submissions (for example contest entries) or without a request from us you send creative ideas, suggestions, proposals, plans, or other materials, whether online, by email, by postal mail, or otherwise (collectively, &lsquo;comments&rsquo;), you agree that we may, at any time, without restriction, edit, copy, publish, distribute, translate and otherwise use in any medium any comments that you forward to us. We are and shall be under no obligation (1) to maintain any comments in confidence; (2) to pay compensation for any comments; or (3) to respond to any comments.</p>\r\n\r\n<p>We may, but have no obligation to, monitor, edit or remove content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party&rsquo;s intellectual property or these Terms of Service.</p>\r\n\r\n<p>You agree that your comments will not violate any right of any third-party, including copyright, trademark, privacy, personality or other personal or proprietary right. You further agree that your comments will not contain libelous or otherwise unlawful, abusive or obscene material, or contain any computer virus or other malware that could in any way affect the operation of the Service or any related website. You may not use a false e-mail address, pretend to be someone other than yourself, or otherwise mislead us or third-parties as to the origin of any comments. You are solely responsible for any comments you make and their accuracy. We take no responsibility and assume no liability for any comments posted by you or any third-party.</p>\r\n\r\n<p><strong>SECTION 10 &ndash; PERSONAL INFORMATION</strong></p>\r\n\r\n<p>Your submission of personal information through the store is governed by our Privacy Policy.</p>\r\n\r\n<p><strong>SECTION 11 &ndash; ERRORS, INACCURACIES AND OMISSIONS</strong></p>\r\n\r\n<p>Occasionally there may be information on our site or in the Service that contains typographical errors, inaccuracies or omissions that may relate to product descriptions, pricing, promotions, offers, product shipping charges, transit times and availability. We reserve the right to correct any errors, inaccuracies or omissions, and to change or update information or cancel orders if any information in the Service or on any related website is inaccurate at any time without prior notice (including after you have submitted your order).</p>\r\n\r\n<p>We undertake no obligation to update, amend or clarify information in the Service or on any related website, including without limitation, pricing information, except as required by law. No specified update or refresh date applied in the Service or on any related website, should be taken to indicate that all information in the Service or on any related website has been modified or updated.</p>\r\n\r\n<p><strong>SECTION 12 &ndash; PROHIBITED USES</strong></p>\r\n\r\n<p>In addition to other prohibitions as set forth in the Terms of Service, you are prohibited from using the site or its content: (a) for any unlawful purpose; (b) to solicit others to perform or participate in any unlawful acts; (c) to violate any international, federal, provincial or state regulations, rules, laws, or local ordinances; (d) to infringe upon or violate our intellectual property rights or the intellectual property rights of others; (e) to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability; (f) to submit false or misleading information; (g) to upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related website, other websites, or the Internet; (h) to collect or track the personal information of others; (i) to spam, phish, pharm, pretext, spider, crawl, or scrape; (j) for any obscene or immoral purpose; or (k) to interfere with or circumvent the security features of the Service or any related website, other websites, or the Internet. We reserve the right to terminate your use of the Service or any related website for violating any of the prohibited uses.</p>\r\n\r\n<p><strong>SECTION 13 &ndash; DISCLAIMER OF WARRANTIES; LIMITATION OF LIABILITY</strong></p>\r\n\r\n<p>We do not guarantee, represent or warrant that your use of our service will be uninterrupted, timely, secure or error-free.</p>\r\n\r\n<p>We do not warrant that the results that may be obtained from the use of the service will be accurate or reliable.</p>\r\n\r\n<p>You agree that from time to time we may remove the service for indefinite periods of time or cancel the service at any time, without notice to you.</p>\r\n\r\n<p>You expressly agree that your use of, or inability to use, the service is at your sole risk. The service and all products and services delivered to you through the service are (except as expressly stated by us) provided &lsquo;as is&rsquo; and &lsquo;as available&rsquo; for your use, without any representation, warranties or conditions of any kind, either express or implied, including all implied warranties or conditions of merchantability, merchantable quality, fitness for a particular purpose, durability, title, and non-infringement.</p>\r\n\r\n<p>In no case shall&nbsp;<strong>CIPHERTEXT TECHNOLOGIES PRIVATE LIMITED</strong>, our directors, officers, employees, affiliates, agents, contractors, interns, suppliers, service providers or licensors be liable for any injury, loss, claim, or any direct, indirect, incidental, punitive, special, or consequential damages of any kind, including, without limitation lost profits, lost revenue, lost savings, loss of data, replacement costs, or any similar damages, whether based in contract, tort (including negligence), strict liability or otherwise, arising from your use of any of the service or any products procured using the service, or for any other claim related in any way to your use of the service or any product, including, but not limited to, any errors or omissions in any content, or any loss or damage of any kind incurred as a result of the use of the service or any content (or product) posted, transmitted, or otherwise made available via the service, even if advised of their possibility. Because some states or jurisdictions do not allow the exclusion or the limitation of liability for consequential or incidental damages, in such states or jurisdictions, our liability shall be limited to the maximum extent permitted by law.</p>\r\n\r\n<p><strong>SECTION 14 &ndash; INDEMNIFICATION</strong></p>\r\n\r\n<p>You agree to indemnify, defend and hold harmless CIPHERTEXT TECHNOLOGIES PRIVATE LIMITED and our parent, subsidiaries, affiliates, partners, officers, directors, agents, contractors, licensors, service providers, subcontractors, suppliers, interns and employees, harmless from any claim or demand, including reasonable attorneys&rsquo; fees, made by any third-party due to or arising out of your breach of these Terms of Service or the documents they incorporate by reference, or your violation of any law or the rights of a third-party.</p>\r\n\r\n<p><strong>SECTION 15 &ndash; SEVERABILITY</strong></p>\r\n\r\n<p>In the event that any provision of these Terms of Service is determined to be unlawful, void or unenforceable, such provision shall nonetheless be enforceable to the fullest extent permitted by applicable law, and the unenforceable portion shall be deemed to be severed from these Terms of Service, such determination shall not affect the validity and enforceability of any other remaining provisions.</p>\r\n\r\n<p><strong>SECTION 16 &ndash; TERMINATION</strong></p>\r\n\r\n<p>The obligations and liabilities of the parties incurred prior to the termination date shall survive the termination of this agreement for all purposes.</p>\r\n\r\n<p>These Terms of Service are effective unless and until terminated by either you or us. You may terminate these Terms of Service at any time by notifying us that you no longer wish to use our Services, or when you cease using our site.</p>\r\n\r\n<p>If in our sole judgment you fail, or we suspect that you have failed, to comply with any term or provision of these Terms of Service, we also may terminate this agreement at any time without notice and you will remain liable for all amounts due up to and including the date of termination; and/or accordingly may deny you access to our Services (or any part thereof).</p>\r\n\r\n<p><strong>SECTION 17 &ndash; ENTIRE AGREEMENT</strong></p>\r\n\r\n<p>The failure of us to exercise or enforce any right or provision of these Terms of Service shall not constitute a waiver of such right or provision.</p>\r\n\r\n<p>These Terms of Service and any policies or operating rules posted by us on this site or in respect to The Service constitutes the entire agreement and understanding between you and us and govern your use of the Service, superseding any prior or contemporaneous agreements, communications and proposals, whether oral or written, between you and us (including, but not limited to, any prior versions of the Terms of Service).</p>\r\n\r\n<p>Any ambiguities in the interpretation of these Terms of Service shall not be construed against the drafting party.</p>\r\n\r\n<p><strong>SECTION 18 &ndash; GOVERNING LAW</strong></p>\r\n\r\n<p>These Terms of Service and any separate agreements whereby we provide you Services shall be governed by and construed in accordance with the laws of India and jurisdiction of Hyderabad, Telangana.</p>\r\n\r\n<p><strong>SECTION 19 &ndash; CHANGES TO TERMS OF SERVICE</strong></p>\r\n\r\n<p>You can review the most current version of the Terms of Service at any time at this page.</p>\r\n\r\n<p>We reserve the right, at our sole discretion, to update, change or replace any part of these Terms of Service by posting updates and changes to our website. It is your responsibility to check our website periodically for changes. Your continued use of or access to our website or the Service following the posting of any changes to these Terms of Service constitutes acceptance of those changes.</p>\r\n\r\n<p><strong>SECTION 20 &ndash; CONTACT INFORMATION</strong></p>\r\n\r\n<p>Questions about the Terms of Service should be sent to us at help@rajubhai.in</p>', 1),
(2, '2021-12-27 22:40:27', '2022-02-06 22:47:18', 'How it works', '<p>RAJUBHAI is simple and easy way to order Food and Groceries online. Enjoy the variety of choices and cuisines which could be delivered to your home or office.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Here is how Rajubhai works:</strong><br />\r\n&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Find a restaurant or store:</strong></p>\r\n\r\n<p>Enter you address or choose from the map on the front page to set your location. Take a review on the restaurants which deliver to your address. Choose a restaurant and dive in its tasty menu.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Choose a dish:</strong></p>\r\n\r\n<p>Choose from the displayed dishes. If there is an option to add products or sauce, for pizza for example, you will be asked for your choice right after you click on the dish. Your order will be dispayed on the right side of the page.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Finish your order and choose type of payment:</strong></p>\r\n\r\n<p>When you complete the order with delicious food, click &quot;Buy&quot;. Now you only have to write your address and choose type of payment as you follow the instructions on the page.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Delivery:</strong></p>\r\n\r\n<p>You will receive SMS as confirmation for your order and information about the delivery time and.....</p>\r\n\r\n<p>That&#39;s all!</p>\r\n\r\n<p>&nbsp;</p>', 1),
(3, '2022-02-06 22:45:23', '2022-02-06 22:45:28', 'Privacy and Policy', '<p>Your privacy is important to us. It is&nbsp;<strong>Ciphertext Technologies Pvt Ltd</strong>&lsquo;s policy to respect your privacy regarding any information we may collect from you across our website,<strong>&nbsp;http://rajubhai.in</strong>, and other sites we own and operate.<br />\r\nWe only ask for personal information when we truly need it to provide a service to you. We collect it by fair and lawful means, with your knowledge and consent. We also let you know why we&rsquo;re collecting it and how it will be used.<br />\r\nWe only retain collected information for as long as necessary to provide you with your requested service. What data we store, we&rsquo;ll protect within commercially acceptable means to prevent loss and theft, as well as unauthorized access, disclosure, copying, use or modification.<br />\r\nWe don&rsquo;t share any personally identifying information publicly or with third-parties, except when required to by law.<br />\r\nOur website may link to external sites that are not operated by us. Please be aware that we have no control over the content and practices of these sites, and cannot accept responsibility or liability for their respective privacy policies.<br />\r\nYou are free to refuse our request for your personal information, with the understanding that we may be unable to provide you with some of your desired services.<br />\r\nYour continued use of our website will be regarded as acceptance of our practices around privacy and personal information. If you have any questions about how we handle user data and personal information, feel free to contact us.<br />\r\nThis policy is effective as of 1 March 2020.</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `paths`
--

CREATE TABLE `paths` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `amount` double(8,2) NOT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'manage restorants', 'web', '2021-12-27 22:40:27', '2021-12-27 22:40:27'),
(2, 'manage drivers', 'web', '2021-12-27 22:40:27', '2021-12-27 22:40:27'),
(3, 'manage orders', 'web', '2021-12-27 22:40:27', '2021-12-27 22:40:27'),
(4, 'edit settings', 'web', '2021-12-27 22:40:27', '2021-12-27 22:40:27'),
(5, 'view orders', 'web', '2021-12-27 22:40:27', '2021-12-27 22:40:27'),
(6, 'edit restorant', 'web', '2021-12-27 22:40:27', '2021-12-27 22:40:27'),
(7, 'edit orders', 'web', '2021-12-27 22:40:27', '2021-12-27 22:40:27'),
(8, 'access backedn', 'web', '2021-12-27 22:40:27', '2021-12-27 22:40:27');

-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

CREATE TABLE `plan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `limit_items` int(11) NOT NULL DEFAULT '0' COMMENT '0 is unlimited',
  `limit_orders` int(11) NOT NULL DEFAULT '0' COMMENT '0 is unlimited',
  `price` double(8,2) NOT NULL,
  `period` int(11) NOT NULL DEFAULT '1' COMMENT '1 - monthly, 2-anually',
  `paddle_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `description` varchar(555) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `features` varchar(555) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `limit_views` int(11) NOT NULL DEFAULT '0' COMMENT '0 is unlimited',
  `enable_ordering` int(11) NOT NULL DEFAULT '1',
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paypal_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mollie_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paystack_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plan`
--

INSERT INTO `plan` (`id`, `name`, `limit_items`, `limit_orders`, `price`, `period`, `paddle_id`, `created_at`, `updated_at`, `deleted_at`, `description`, `features`, `limit_views`, `enable_ordering`, `stripe_id`, `paypal_id`, `mollie_id`, `paystack_id`) VALUES
(1, 'Free', 30, 100, 0.00, 1, '', '2021-12-27 22:40:30', '2021-12-27 22:40:30', NULL, 'If you run a small restaurant or bar, or just need the basics, this plan is great.', 'Accept 100 Orders/m, Access to the menu creation tool, Unlimited views, 30 items in the menu, Community support', 0, 1, '', NULL, NULL, NULL),
(2, 'Starter', 0, 600, 99.00, 1, '', '2021-12-27 22:40:30', '2021-12-27 22:40:30', NULL, 'For bigger restaurants and bars. Offer a full menu.', 'Accept 600 Orders/m, Access to the menu creation tool, Unlimited views, Unlimited items in the menu, Dedicated Support', 0, 1, '', NULL, NULL, NULL),
(3, 'Pro', 0, 0, 120.00, 1, '', '2021-12-27 22:40:30', '2021-12-27 22:40:30', NULL, 'Accept orders directly from your customer mobile phone', 'Featured on our site, Accept unlimited Orders, Manage order, Full access to QR tool, Access to the menu creation tool, Unlimited views, Unlimited items in the menu, Dedicated Support', 0, 1, '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `post_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rating` int(11) NOT NULL,
  `rateable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rateable_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `comment` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `created_at`, `updated_at`, `rating`, `rateable_type`, `rateable_id`, `user_id`, `order_id`, `comment`) VALUES
(17, '2022-02-27 04:08:46', '2022-02-27 04:08:46', 5, 'App\\Restorant', 20, 39, 18, 'Good');

-- --------------------------------------------------------

--
-- Table structure for table `restoareas`
--

CREATE TABLE `restoareas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restaurant_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2021-12-27 22:40:27', '2021-12-27 22:40:27'),
(2, 'owner', 'web', '2021-12-27 22:40:27', '2021-12-27 22:40:27'),
(3, 'driver', 'web', '2021-12-27 22:40:27', '2021-12-27 22:40:27'),
(4, 'client', 'web', '2021-12-27 22:40:27', '2021-12-27 22:40:27'),
(5, 'staff', 'web', '2021-12-27 22:40:27', '2021-12-27 22:40:27');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(8, 1),
(5, 2),
(6, 2),
(8, 2),
(7, 3),
(8, 3);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `site_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `search` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `restorant_details_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `restorant_details_cover_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `header_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `header_subtitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USD',
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `playstore` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `appstore` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `maps_api_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `delivery` double(8,2) NOT NULL DEFAULT '0.00',
  `typeform` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `mobile_info_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `mobile_info_subtitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_options` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '{}',
  `site_logo_dark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_fields` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `created_at`, `updated_at`, `site_name`, `site_logo`, `search`, `restorant_details_image`, `restorant_details_cover_image`, `description`, `header_title`, `header_subtitle`, `currency`, `facebook`, `instagram`, `playstore`, `appstore`, `maps_api_key`, `delivery`, `typeform`, `mobile_info_title`, `mobile_info_subtitle`, `order_options`, `site_logo_dark`, `order_fields`) VALUES
(1, '2021-12-27 22:40:27', '2022-02-24 21:50:23', 'Rajubhai Food and Groceries', '5d3a6c13-b4bd-4bec-914e-8c0a11d19a59', '/default/cover.jpg', '3743cd00-885f-4bfb-9758-2e59495d7083', '/default/cover.jpg', 'Food and Groceries Delivery from best stores', 'Food Delivery from<br /><b>Hyderabad’s</b> Best Restaurants', 'We deliver happiness to your home.', 'USD', '', '', '', '', 'AIzaSyCZhq0g1x1ttXPa1QB3ylcDQPTAzp_KUgA', 0.00, '', 'Download the food you love', 'It`s all at your fingertips - the restaurants you love. Find the right food to suit your mood, and make the first bite last. Go ahead, download us.', '{}', '/default/logo.png', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `simple_delivery_areas`
--

CREATE TABLE `simple_delivery_areas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` double(8,2) NOT NULL,
  `restaurant_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sms_verifications`
--

CREATE TABLE `sms_verifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `name`, `alias`) VALUES
(1, 'Just created', 'just_created'),
(2, 'Accepted by admin', 'accepted_by_admin'),
(3, 'Accepted by restaurant', 'accepted_by_restaurant'),
(4, 'Assigned to driver', 'assigned_to_driver'),
(5, 'Prepared', 'prepared'),
(6, 'Picked up', 'picked_up'),
(7, 'Delivered', 'delivered'),
(8, 'Rejected by admin', 'rejected_by_admin'),
(9, 'Rejected by restaurant', 'rejected_by_restaurant'),
(10, 'Updated', 'updated'),
(11, 'Closed', 'closed'),
(12, 'Rejected by driver', 'rejected_by_driver'),
(13, 'Accepted by driver', 'accepted_by_driver');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subscription_items`
--

CREATE TABLE `subscription_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subscription_id` bigint(20) UNSIGNED NOT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE `tables` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(11) NOT NULL DEFAULT '4',
  `restoarea_id` bigint(20) UNSIGNED DEFAULT NULL,
  `restaurant_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `x` double(8,2) DEFAULT NULL,
  `y` double(8,2) DEFAULT NULL,
  `w` double(8,2) DEFAULT NULL,
  `h` double(8,2) DEFAULT NULL,
  `rounded` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `google_id` char(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb_id` char(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `verification_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_verified_at` timestamp NULL DEFAULT NULL,
  `plan_id` bigint(20) UNSIGNED DEFAULT NULL,
  `plan_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cancel_url` varchar(555) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `update_url` varchar(555) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `checkout_id` varchar(555) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `subscription_plan_id` varchar(555) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `stripe_account` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `birth_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `working` int(11) NOT NULL DEFAULT '1',
  `onorder` int(11) DEFAULT NULL,
  `numorders` int(11) NOT NULL DEFAULT '0',
  `rejectedorders` int(11) NOT NULL,
  `paypal_subscribtion_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mollie_customer_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mollie_mandate_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_percentage` decimal(6,4) NOT NULL DEFAULT '0.0000',
  `extra_billing_information` text COLLATE utf8mb4_unicode_ci,
  `mollie_subscribtion_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paystack_subscribtion_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paystack_trans_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `restaurant_id` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `google_id`, `fb_id`, `name`, `email`, `email_verified_at`, `password`, `api_token`, `phone`, `remember_token`, `created_at`, `updated_at`, `active`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`, `verification_code`, `phone_verified_at`, `plan_id`, `plan_status`, `cancel_url`, `update_url`, `checkout_id`, `subscription_plan_id`, `stripe_account`, `birth_date`, `lat`, `lng`, `working`, `onorder`, `numorders`, `rejectedorders`, `paypal_subscribtion_id`, `mollie_customer_id`, `mollie_mandate_id`, `tax_percentage`, `extra_billing_information`, `mollie_subscribtion_id`, `paystack_subscribtion_id`, `paystack_trans_id`, `restaurant_id`, `deleted_at`) VALUES
(1, NULL, NULL, 'Admin Rajubhai', 'ynreddy1989@gmail.com', '2021-12-27 22:40:27', '$2y$10$A7/6no1MPfithVpAGtRS7ewIDfofQ.69pocyAz0w975vhYSf0DnhS', 'ieW4k3FBVOD4o7iuL3GhoZWm1sXQe3sgF86MHfQuoV3NhqrvHVNLZuDoSC0dAXzqdclgDQnhPpEoGa52', '9440995080', 'Z9IF3AbvFGkjSPXDBaiJv7HgTR8k1UWQVpt4GVV5XS9gZLA2w6kz7UKlSG1j', '2021-12-27 22:40:27', '2022-02-06 20:44:33', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, NULL, 'Demo Owner', 'owner@example.com', '2021-12-27 22:40:27', '$2y$10$pFXIlljRBEtpswkBkEb5FeLmZAGH2.BYdzVqDHRp4PFHMhMOLa32C', 'iZD918apcz1Vp8JKrn74b2Z0cA6J8nBT5eRKUv5i1tiPIqMJqxPR7aeZHukiji6ONT4XXiPIM61iieSR', '', 'fnwIO56SNEYh9ufP85XRxRMeeqi9i5F9LXVLrUNj751n0QdQqvGkg70mdVdP', '2021-12-27 22:40:27', '2021-12-27 22:40:27', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, NULL, 'Demo Staff 1', 'staff@example.com', '2021-12-27 22:40:27', '$2y$10$5XlsQgPvdz7CpcSKX3sSmOMd8XG8JK9vqhSR2cm7YduVw0PxoaY6e', 'KDpefp6a8lgKGpDRkHWIB4mYsA46xYxhHBmhQ6cBoghq05L0qwivBvnNaXZgcWVolod4UlS1axt4FjPn', '', NULL, '2021-12-27 22:40:27', '2021-12-27 22:40:27', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 1, NULL),
(4, NULL, NULL, 'Demo Staff 2', 'staff2_1@example.com', '2021-12-27 22:40:27', '$2y$10$OIko.dN9bIfBPAJwVD6OKO1ziL7t2PuSIfeeCKtJGqxK6iedNC4jK', 'twMwmtWX6huioB6XYTQKPsOHL3tl0E22v6qsOM4i3Yh14T16xvtb30DTaaAtjeeN8E7bo9q0XIkNS64x', '', NULL, '2021-12-27 22:40:27', '2021-12-27 22:40:27', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 1, NULL),
(5, NULL, NULL, 'Demo Staff 1', 'staff1_2@example.com', '2021-12-27 22:40:27', '$2y$10$TT/CzfB/m.Q/fgVbCwHd9uJz1aLDzZeKa.4lv/idZIx5J9gNxWMIi', 'kTH50Wwe9ttzGxgQM4GrsqFSvLGwE9Em0o3U50BoCdGeY6s5FNezy5dob9UawfDQSpMPCkVAVSim32Rb', '', NULL, '2021-12-27 22:40:27', '2021-12-27 22:40:27', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 2, NULL),
(6, NULL, NULL, 'Demo Staff 2', 'staff2_2@example.com', '2021-12-27 22:40:27', '$2y$10$2KewPd.eUpkbSMNNpGYJPOIrI130kH7xC9HI1QXmGGdEz6D3aOx3a', 'OBWbk4vrKxoXyaRgfQ6b3WRcQruWpxex2RukeN63h9AMEofRf5ty2zGSmt5wgdVT1EHnODMrCOcE7y1b', '', NULL, '2021-12-27 22:40:27', '2021-12-27 22:40:27', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 2, NULL),
(7, NULL, NULL, 'Demo Staff 1', 'staff1_3@example.com', '2021-12-27 22:40:27', '$2y$10$H3vqN1F4ekqw40HUg7JnYO/7drbv.miv4jtEtyEjFtKSuANev.Taa', 'yqS7m1VUXzv7Ou5Qvv9cztHMX9UmJsQ4PvsQ4MRmSJctwppaj1JcOZTG3RRQ6o8MCs1tb1KFu1m1oNAo', '', NULL, '2021-12-27 22:40:27', '2021-12-27 22:40:27', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 3, NULL),
(8, NULL, NULL, 'Demo Staff 2', 'staff2_3@example.com', '2021-12-27 22:40:27', '$2y$10$rA4dxinhk5XDvDnSyIaKS.9oyEHOuTOS9c7h1yp.T1obQdQO98wDW', 'gVgDbdvsTjD7I6g77vs4a3yq2A7yaxemmFFOgGv1cUYuGuDp3N3HSfx68aHz2RtYbNuGUTcBo6R9YvlK', '', NULL, '2021-12-27 22:40:27', '2021-12-27 22:40:27', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 3, NULL),
(9, NULL, NULL, 'Demo Staff 1', 'staff1_4@example.com', '2021-12-27 22:40:27', '$2y$10$cf8OfECYTQKr7cCVIFObcuWLpBNAcDEipzqehu63w89q6iEJVEbO.', 'BEvTdEV6hmdKEXLdVg4Lh5YwdYbIs2aYnWtnpyIufZM5JeWutbs0lFMhCfBa3GlnRLpQW2SqGZkkmh5u', '', NULL, '2021-12-27 22:40:27', '2021-12-27 22:40:27', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 4, NULL),
(10, NULL, NULL, 'Demo Staff 2', 'staff2_4@example.com', '2021-12-27 22:40:27', '$2y$10$KxtuSNw3tP/My5fRBbiccOq.UEa5DRVVsLohuGHUM.I1WGW9FU/py', 'cvU8NxnnE2sk14DJzFmd1Q0bZYoof2Ay9ne2pVHgURalcyxhz3iJagAAEccHk4PWBxnhPx8VMN1K5PMk', '', NULL, '2021-12-27 22:40:27', '2021-12-27 22:40:27', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 4, NULL),
(11, NULL, NULL, 'Demo Staff 1', 'staff1_5@example.com', '2021-12-27 22:40:28', '$2y$10$h1mRZHyewtIfzkJ/zd0VKeHztSxlNoPsyMGXFv5eSlaUdiJgGm1/a', 'Ka5UCM5YOHO3cEPogp7ZHozqQrzBnAby11l2KVvwrEv1waia3MVDIWH8OOjNJOPT5Dxz2PwjfijdgIql', '', NULL, '2021-12-27 22:40:28', '2021-12-27 22:40:28', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 5, NULL),
(12, NULL, NULL, 'Demo Staff 2', 'staff2_5@example.com', '2021-12-27 22:40:28', '$2y$10$3R1iaM8Jh4/If01.5yWPaukLkYF8myI1CuDm99QTV9.VqYbVQmcvW', '9eZj890B5R79LDiw0wbNHDVdm6o791IA4c9SRYeMTjyDXpE9f6r4i1dpTJiFzOFFXF4nDO80fkXc0sQM', '', NULL, '2021-12-27 22:40:28', '2021-12-27 22:40:28', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 5, NULL),
(13, NULL, NULL, 'Demo Staff 1', 'staff1_6@example.com', '2021-12-27 22:40:28', '$2y$10$wVMryj8oNDEuDM7pbostEuaxZPZDtzJwjUKjDzCt/AZOfXDyapfqa', 'JOzV2lXqNjZm95iekphOEa10uoanu2qBVi1eSWwg5TLRR8LSmbTncRyH99LsgHphNmAPD5VdbWcNRDeT', '', NULL, '2021-12-27 22:40:28', '2021-12-27 22:40:28', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 6, NULL),
(14, NULL, NULL, 'Demo Staff 2', 'staff2_6@example.com', '2021-12-27 22:40:28', '$2y$10$YIhit9yMhsM4uWWxrc8oGuPTTHblFs9.mvhhXPsGG9QXTBjYk7RDG', 'xZKcLhyWecWfVkz5VPDV3qr8tgoBpxFcgmzW4BZONpbkNxVzHFUdYyAiQG5VXQRh7SrzcoKVUuBcXlsY', '', NULL, '2021-12-27 22:40:28', '2021-12-27 22:40:28', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 6, NULL),
(15, NULL, NULL, 'Demo Staff 1', 'staff1_7@example.com', '2021-12-27 22:40:28', '$2y$10$qyAH7HHq6WqQF2yiABPzP.yiu6FL6/umqBoGXsA19o6kRwjIEQcsK', 'zssIOg7mAM5mmoVmCYF9juJw5ImjV8X0zUY7jMd7lMQAmzAkzwUR8y1GkPuI27EnuNCQPU1ov8HbevJU', '', NULL, '2021-12-27 22:40:28', '2021-12-27 22:40:28', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 7, NULL),
(16, NULL, NULL, 'Demo Staff 2', 'staff2_7@example.com', '2021-12-27 22:40:28', '$2y$10$jJjGvBEhOXr1ZACtsvXf1ekA/G4wgiT4AjKQPEkgQX7NHFxYsH5cy', 'N4bnGkp8Pmicj7NhGVZELf2G7yS0n8dedMY0M8BLYeMx1JKCt5e3SdraDDzTVuUf0kNp4Q7ISK3a6v8Z', '', NULL, '2021-12-27 22:40:28', '2021-12-27 22:40:28', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 7, NULL),
(17, NULL, NULL, 'Demo Staff 1', 'staff1_8@example.com', '2021-12-27 22:40:28', '$2y$10$reIIrsv3ItC83FThultb2u9b2EgKs/u9TMwqr/h7idbx//s8B8rC6', 'paeKFm6vJSwQ5P8kuDfogDpXbXj4fXj7YlQDhoQaQfoCUKv0A2euwSTLLVaU3wk779PMeyp1GsFoPx2V', '', NULL, '2021-12-27 22:40:28', '2021-12-27 22:40:28', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 8, NULL),
(18, NULL, NULL, 'Demo Staff 2', 'staff2_8@example.com', '2021-12-27 22:40:28', '$2y$10$eMaxNJLdIaEX/SRS4/rBxeEkO55pfzo/TKJcKbBsPq5terrxPD1p.', '4iXyvcHdpAVkKA2eX09ovi3fRnteYaP0jMpetzkeyHtIdIWPfUcmhuls9vmNb5UTTcJ9U0CIkpslZFZY', '', NULL, '2021-12-27 22:40:28', '2021-12-27 22:40:28', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 8, NULL),
(19, NULL, NULL, 'Demo Staff 1', 'staff1_9@example.com', '2021-12-27 22:40:28', '$2y$10$Cu.iXtzEtkk5CMqjQdBIOusPwPahsgcqkOHEnj72WPWDnjLZafGy2', 'VVPkwBJNTq1AgzIUelXZjb7ttgpjwGg1oDo4tIBQUqsXLfnwVvcWbLuILvPUlLxARgfAeQge9Kcrk2Tx', '', NULL, '2021-12-27 22:40:28', '2021-12-27 22:40:28', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 9, NULL),
(20, NULL, NULL, 'Demo Staff 2', 'staff2_9@example.com', '2021-12-27 22:40:29', '$2y$10$7SEhw1hPl/8.gjmRbe50e.ZhGubiPfY/d5CEGlZdl4nJCHrrrR5Pm', '585eL9rFgIQNsgFvquch3wNolMGOiv1yie0c1OD0sYii7FNOIOdHJEkPKnXEc3IIF91SMC9iEkd3LDi5', '', NULL, '2021-12-27 22:40:29', '2021-12-27 22:40:29', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 9, NULL),
(21, NULL, NULL, 'Demo Staff 1', 'staff1_10@example.com', '2021-12-27 22:40:29', '$2y$10$0SnUHN4MnGR6EMbeD/Rtt.AgQNwym9y74/kYzNLVvwRFjW02PsIaq', '18iZSEhEPCoUEIajLuzqfJIqJimKpFpfhRvq1tVuCVc0bWURCt8aQuJBdrsEeLnOvt17QW2ILr3CiQLr', '', NULL, '2021-12-27 22:40:29', '2021-12-27 22:40:29', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 10, NULL),
(22, NULL, NULL, 'Demo Staff 2', 'staff2_10@example.com', '2021-12-27 22:40:29', '$2y$10$i6wfvM.GOPo1LzthNaIZ1ewrfeDFhLnS0xNr314y.i4Y1I9iW0agS', 'jcbKZ0L0vmg6iWLq49EafKokDcLMP5Ufvp96oHl1ikiBytD4XAxvoXLp7SaphaoecCmOsOUHuswymt8G', '', NULL, '2021-12-27 22:40:29', '2021-12-27 22:40:29', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 10, NULL),
(23, NULL, NULL, 'Demo Staff 1', 'staff1_11@example.com', '2021-12-27 22:40:29', '$2y$10$GBe0n0Iyz2hj/hj2kbjMWONvHKkGPrLhHMnTRN07b.IJtxChKpXoW', '8iRZPQjj9CyjX43HHeRWkZlnKcl8sOXJkc1Re5CY3pVSR5mnKw45jwlXOd6oPIg2YasYi4olIWRO9evh', '', NULL, '2021-12-27 22:40:29', '2021-12-27 22:40:29', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 11, NULL),
(24, NULL, NULL, 'Demo Staff 2', 'staff2_11@example.com', '2021-12-27 22:40:29', '$2y$10$Gaqea9fm81zUYyYayPkwxe6UUef0avqV.13FhAs30TcMM31zPpeai', 'Wy2u8I8yx22q1YHIo10ww5EHVPo8d0m24hw1TNwCzDmFY1FKj6E382G81bmhHtUSIjXaL3Z9lwCgzCnc', '', NULL, '2021-12-27 22:40:29', '2021-12-27 22:40:29', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 11, NULL),
(25, NULL, NULL, 'Demo Staff 1', 'staff1_12@example.com', '2021-12-27 22:40:29', '$2y$10$YAVeJb/oin1W1RFSKhf/burzCPwe2AsTMQR1sfgefU74.4s6C1lkq', 'myDInwpKx79wpoIyfG5zIo3AGyweR4ncAzy6mQqm9iMPGSGUdTQrdMdxNGSBWpedtzG10NvgmcF6pYud', '', NULL, '2021-12-27 22:40:29', '2021-12-27 22:40:29', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 12, NULL),
(26, NULL, NULL, 'Demo Staff 2', 'staff2_12@example.com', '2021-12-27 22:40:29', '$2y$10$lH8bCreIv1MKeWvrF76wi.veFAmH35QpzbCiAnSrmBcc2Csdq7Zo.', 'a4c9v2kLKNEpZ40jW4Uw83dlzNGhaLbMPJS4FZnB3dLiJSO2zTta69A8YgsJkbhlS07Q9Ss4nloD3hkf', '', NULL, '2021-12-27 22:40:29', '2021-12-27 22:40:29', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 12, NULL),
(27, NULL, NULL, 'Demo Staff 1', 'staff1_13@example.com', '2021-12-27 22:40:29', '$2y$10$B6IKGHKVArklcG8o5QAwJuh7fFGLsi4Jl7T8Pf1FdtBUS4gSxzi.q', 'b9SebkR67Ecl2rkew53k8VfeINilH6Fwpbsc89rXsbl5sJVN4mg0KYmKOYolMD0bQJsWYX9NHaNdjmQC', '', NULL, '2021-12-27 22:40:29', '2021-12-27 22:40:29', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 13, NULL),
(28, NULL, NULL, 'Demo Staff 2', 'staff2_13@example.com', '2021-12-27 22:40:29', '$2y$10$gOCdKGf8xrSdj0HTX9vnO.m1A4nhiRg2nZMgDLpUm9oFzWgZqHCge', 'AK1tSrnq5PlqCjurkvyTyqAfWddnIlrHUqlsJ6rCN31LMHrDpMMcnsRWGcO5IMx0wtgMANkP5ia3Gkws', '', NULL, '2021-12-27 22:40:29', '2021-12-27 22:40:29', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 13, NULL),
(29, NULL, NULL, 'Demo Staff 1', 'staff1_14@example.com', '2021-12-27 22:40:30', '$2y$10$yh6J/RA8B0NC8cggmFNAcu822GF8fMFulWTp3gYOxcIkhTQ5phhUO', 'Kc8zVbpoat4IRGt1z1vIVBeg9LGQPmodmB8ti51DhbqYIqrBe8IPQdqag38HtEkBAEHVUhshJHucVasz', '', NULL, '2021-12-27 22:40:30', '2021-12-27 22:40:30', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 14, NULL),
(30, NULL, NULL, 'Demo Staff 2', 'staff2_14@example.com', '2021-12-27 22:40:30', '$2y$10$OqeaH6fSsWATNUf9Fc7jkuKRDuaZlgSNysJjaF4YHB5DzeAOQ.csC', 'juFRsGXMI1SChFD9GiH2m1vByHGSQSxKSyKHw4Hmywj6zCNxttfDHCo0nI0VTLwnR3BYerOAJv965wg5', '', NULL, '2021-12-27 22:40:30', '2021-12-27 22:40:30', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 14, NULL),
(31, NULL, NULL, 'Demo Staff 1', 'staff1_15@example.com', '2021-12-27 22:40:30', '$2y$10$JIbUOBxraOrccI.BhdlSGOPEExm30dINslKBEWxLc./hMErd0bPRi', 'KPf4lh6CRXqCLeaSQXB5tzjo8LLcg5robXaQJZYXFzh7ZEk8mWYFG4fl2pWHK5hrwlZlwj7hIgJXZuRg', '', NULL, '2021-12-27 22:40:30', '2021-12-27 22:40:30', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 15, NULL),
(32, NULL, NULL, 'Demo Staff 2', 'staff2_15@example.com', '2021-12-27 22:40:30', '$2y$10$0KUvjqTHBa1PucQ357vEmeQJf845CojtNeS2ef7kKs27hnJyXUb4i', 'QryJYaLOhCoba2f9TRot9gmii4SpSrBQgR4dVJrIDbygonIkhkiwfDQplra9ScgFV6Np9IOlb9kaU6w9', '', NULL, '2021-12-27 22:40:30', '2021-12-27 22:40:30', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 15, NULL),
(33, NULL, NULL, 'Demo Staff 1', 'staff1_16@example.com', '2021-12-27 22:40:30', '$2y$10$9sQscRviKHGYJmm/nWSX3eCng/R3s754F9TVEf.OfUmaF4TeAWnfe', 'kD3awdkft13zwvkGTg5y8uk6akMN1brUMieg6rI8vUf7RgdPDTuweHuG5DLJckyVGUBmZkOhzSlbD6BJ', '', NULL, '2021-12-27 22:40:30', '2021-12-27 22:40:30', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 16, NULL),
(34, NULL, NULL, 'Demo Staff 2', 'staff2_16@example.com', '2021-12-27 22:40:30', '$2y$10$ZvkdehwKBlJZykV8Rc5IJ./RXbGGhptqnBS2.pV3w4Iz.RfrvdSUG', 'nycKhaIBusFRheVupFCZsftxAW1nCMAGiDTBx13iJtqiXyHceaSWOVA7ZfddZKySzLFsdq9NPwa4FH1o', '', NULL, '2021-12-27 22:40:30', '2021-12-27 22:40:30', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 16, NULL),
(35, NULL, NULL, 'Demo Driver', 'driver@example.com', '2021-12-27 22:40:30', '$2y$10$z2uUMehDfZatnFgb36aEZe2fqmEIaYfB5NAXlE82Hpj5YqTVi6nZ.', 'XZk6tPhKjM7FEi290Zd4X1gVjVkfeJBXrBfNvVJmSaDCcB6v7CQS8rhfllNyQm7hECGdTlwGtWzfKfow', '', NULL, '2021-12-27 22:40:30', '2022-01-22 20:20:08', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 1, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, NULL, NULL),
(36, NULL, NULL, 'Demo Client 1', 'client@example.com', '2021-12-27 22:40:30', '$2y$10$9g2AJvhw8nDjJuNRwnNPGuKojmnOteFouiyGIsVUNKpcGdM0rck.C', 'cjJ8HdUuvBjOGVcQ1gvwDlqOsoddmGoWEu5W76Q2uijZzMspLkBW8Y9F84JX3gKpAsnJveYRzLT5ySRQ', '', NULL, '2021-12-27 22:40:30', '2021-12-27 22:40:30', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, NULL, NULL),
(37, NULL, NULL, 'Demo Client 2', 'client2@example.com', '2021-12-27 22:40:30', '$2y$10$QFEx6.jJWbDu3jkZ3BNLPe0XBFM6ba.r7A9ZZxCLL5Qg9y/7WQNMa', 'nVoqlEaAb2v3EtVDKWDesMsG1TjsHN9Etkx7zuQNtgBDl4YePrpXFd1S1EMBE4DC9wsRtaQxaPzB1yfr', '', NULL, '2021-12-27 22:40:30', '2021-12-27 22:40:30', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, NULL, NULL),
(38, NULL, NULL, 'Demo Driver 2', 'driver2@example.com', '2021-12-27 22:40:30', '$2y$10$XiEXv7ppoV3EKr.9mXK.9.whiHdb0fiwV4hbZAkQndFK/HNPBFYlq', 'ppYv172LgRXyNoVX9O26YvN3chs15HNt9ICfC0vHAcNjOhCBbcD1YaXsiD3Mhynkc2sFRKgFsRpOxhLG', '', NULL, '2021-12-27 22:40:30', '2021-12-27 22:40:30', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, NULL, NULL),
(39, NULL, NULL, 'Venkat', 'kvraju1211@gmail.com', NULL, '$2y$10$5/dEBqKscTQZhXv1/a4qX.LJ0zIAl9EiYoEGUMAhfpx0FARIS4JsW', 'eUVyQsotG53oFV86kBXcvpijXQ9gLsMtw6BPWrOCGx5NwrjqeMz0k79DNTvHbnI2YU4lheCWMJnIs1Y7', '9440995080', '7yAG0tE4s5MJUE15YpL0ZY9eMCQLHUmYfUOHw6mgEErJN9rtLp4WGw9seOYb', '2022-02-06 20:33:29', '2022-02-06 20:33:29', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, NULL, NULL),
(40, NULL, NULL, 'Ganesh', 'kvrajuventures@gmail.com', NULL, '$2y$10$WV4fhimetSyI.ZGxsg9/meCBkxB3h1eSpSgHa3Rjh5qLgLTVRmeU2', 'ueabHzXKF1BoLIbZi1pWgqinOQqyhH5FdcOhpAq9OtLSDWkYWHUYRbXGsrge0vfxYiuAnMBdtQp2NHs2', '9036789012', 'HUCQoqA23n0PW7ymqScbmEzQyMsLbv3tB4VlkWUZqtSiIsOn5SEapEWxyCGv', '2022-02-06 20:37:21', '2022-02-06 20:38:50', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, NULL, NULL),
(41, NULL, NULL, 'Raju', 'kandukuri.v.raju@gmail.com', NULL, '$2y$10$vZxcQueP/IGd0FUytbhqW.EOZkwGINwDRWSHBcgPfKeXPSot1mD6S', 'QnhoVxL3QLCZu18lHWX3sbcjxauQc6zpfrzAboTEWfSJsUhTaTOHjchP8Cw0nlWlfMZyPZmUmDjQgxfQ', '9036789012', NULL, '2022-02-06 22:48:53', '2022-02-06 22:48:53', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, NULL, NULL),
(42, NULL, NULL, 'Naga', 'kasuswapna21@gmail.com', NULL, '$2y$10$XgYhamnXUspgeHWA59g6pu0KyFoySh/5WLnIapoXaZ.usdyx.R3My', 'MWAdWVfWB2Smwu9a83e7bW3FQNuBiIp3pSKY8Sj3x2gFj0TuSpWn00HrKujVqyaoYGYfnVtyHdJ2stgb', '9676326502', NULL, '2022-02-15 08:03:01', '2022-02-15 08:03:01', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, NULL, NULL),
(43, NULL, NULL, 'Nga Reddy', 'nag@gmail.com', NULL, '$2y$10$BVTUrbdO9g5f8wn5UxM/GucEOD5qxM9uoVZ3xt0093nHUBeHYsmeC', '409mpUAqRXXs6v6gi3e8bHkxtqZSeJxjjpnZEPsm8Qr1qvTGm7PRoJyqvio3n873QGJfvA0K7u8L1Dq7', '9676326502', NULL, '2022-02-20 23:29:05', '2022-02-20 23:29:05', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, NULL, NULL),
(44, NULL, NULL, 'Raju', 'venkata.r.kandukuri@gmail.com', NULL, '$2y$10$ubaot4DD8UH5oJy.g9Tg8errS.geBWxW4gX9MZq4zuP4C5e7GK8IC', '57H76aRoke6jeCrGh0RKsGNJ0O9wypuHb2mx4afebu49RiwvIORn7U54rDSBuSCkjSoaaAeDRSbOTRN3', '9036789012', '7hHiW6TTaj9pNDxznivIrfVs9JvXawtCeo80mtKQViHJGyEE41iXt8dzj1tT', '2022-02-24 16:46:59', '2022-02-25 00:13:12', 1, NULL, NULL, NULL, NULL, NULL, NULL, 3, 'set_by_admin', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, NULL, NULL),
(45, NULL, NULL, 'Muppidi Bangarraju', 'bangarrajumuppidu@gmail.com', NULL, '$2y$10$skgyHlYNpwuPOZV5P5Ia5uwkc/CDFpJUTlq413K5ruhoSnNLEh.SS', 'kzqnG3HeNhkLxwmDV90eMrpfPRxESar9SCPVTB7cw2VSYLvNXa9F90KzomJBbUsWLzEE8Skl2KVqdzkr', '+919398390862', NULL, '2022-02-26 21:15:54', '2022-02-26 21:15:54', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, NULL, NULL),
(46, NULL, NULL, 'Santosh', 'kvraju@ciphertext.in', NULL, '$2y$10$RfoTjMFn4w5gzNX9SmyXAeAvKPmww1hpOBuGdLTQyr6oI.Wc6TU2G', 'Gyo6hnl41F2EMOyXXpCNF8vr0HKyO8z5ke8aajgBhm9SMcct1RUmDjYS4eO0TtCWaoZnQszPYQZpesc6', '+919949853126', '0HaMfmc72t8dUCpZu0ucNX5ow6QNvnbLl0UlyJx5COx5UIAF06QoJ0IGSAFy', '2022-02-27 03:20:17', '2022-02-27 03:33:45', 1, NULL, NULL, NULL, NULL, NULL, NULL, 3, 'set_by_admin', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, 20, NULL),
(47, NULL, NULL, 'Himanshu Shekhar', 'manjeshkumar39@gmail.com', NULL, '$2y$10$xFKAFGsVak6MKB1XrVU1Xeav6bxUhjotCTvFmh/bmta/WNoZGfrBO', 'ElpO6v3dD8SG7Eyv3mcmq8mJsyOIDKqURM0jB6zbx7VviWy2RvQOVW3qPPzgJLuBjowWm4xD2kZwWhka', '+917989317594', NULL, '2022-02-28 00:12:25', '2022-02-28 00:12:25', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, 0.0000, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `variants`
--

CREATE TABLE `variants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL,
  `options` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `enable_qty` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_system` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `variants_has_extras`
--

CREATE TABLE `variants_has_extras` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `variant_id` bigint(20) UNSIGNED NOT NULL,
  `extra_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visits`
--

CREATE TABLE `visits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `restaurant_id` bigint(20) UNSIGNED NOT NULL,
  `table_id` bigint(20) UNSIGNED DEFAULT NULL,
  `by` int(11) NOT NULL DEFAULT '1' COMMENT '1 - Owner, 0 - Client Himself',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `address_user_id_foreign` (`user_id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banners_vendor_id_foreign` (`vendor_id`),
  ADD KEY `banners_page_id_foreign` (`page_id`);

--
-- Indexes for table `cart_storage`
--
ALTER TABLE `cart_storage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_storage_id_index` (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_restorant_id_foreign` (`restorant_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cities_alias_unique` (`alias`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `restorants_name_unique` (`name`),
  ADD UNIQUE KEY `restorants_subdomain_unique` (`subdomain`),
  ADD KEY `restorants_user_id_foreign` (`user_id`);

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coupons_code_unique` (`code`),
  ADD KEY `coupons_restaurant_id_foreign` (`restaurant_id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_restaurant_id_foreign` (`restaurant_id`),
  ADD KEY `expenses_expenses_category_id_foreign` (`expenses_category_id`),
  ADD KEY `expenses_expenses_vendor_id_foreign` (`expenses_vendor_id`);

--
-- Indexes for table `expenses_category`
--
ALTER TABLE `expenses_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_category_restaurant_id_foreign` (`restaurant_id`);

--
-- Indexes for table `expenses_vendor`
--
ALTER TABLE `expenses_vendor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_vendor_restaurant_id_foreign` (`restaurant_id`);

--
-- Indexes for table `extras`
--
ALTER TABLE `extras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `extras_item_id_foreign` (`item_id`);

--
-- Indexes for table `hours`
--
ALTER TABLE `hours`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hours_restorant_id_foreign` (`restorant_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `items_category_id_foreign` (`category_id`);

--
-- Indexes for table `item_has_allergens`
--
ALTER TABLE `item_has_allergens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `localmenus`
--
ALTER TABLE `localmenus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `localmenus_restaurant_id_foreign` (`restaurant_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `options_item_id_foreign` (`item_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_address_id_foreign` (`address_id`),
  ADD KEY `orders_client_id_foreign` (`client_id`),
  ADD KEY `orders_restorant_id_foreign` (`restorant_id`),
  ADD KEY `orders_driver_id_foreign` (`driver_id`),
  ADD KEY `orders_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `order_has_items`
--
ALTER TABLE `order_has_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_has_items_order_id_foreign` (`order_id`),
  ADD KEY `order_has_items_item_id_foreign` (`item_id`);

--
-- Indexes for table `order_has_status`
--
ALTER TABLE `order_has_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_has_status_order_id_foreign` (`order_id`),
  ADD KEY `order_has_status_status_id_foreign` (`status_id`),
  ADD KEY `order_has_status_user_id_foreign` (`user_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `paths`
--
ALTER TABLE `paths`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_user_id_foreign` (`user_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_rateable_type_rateable_id_index` (`rateable_type`,`rateable_id`),
  ADD KEY `ratings_rateable_id_index` (`rateable_id`),
  ADD KEY `ratings_rateable_type_index` (`rateable_type`),
  ADD KEY `ratings_user_id_foreign` (`user_id`),
  ADD KEY `ratings_order_id_foreign` (`order_id`);

--
-- Indexes for table `restoareas`
--
ALTER TABLE `restoareas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `restoareas_restaurant_id_foreign` (`restaurant_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `simple_delivery_areas`
--
ALTER TABLE `simple_delivery_areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `simple_delivery_areas_restaurant_id_foreign` (`restaurant_id`);

--
-- Indexes for table `sms_verifications`
--
ALTER TABLE `sms_verifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sms_verifications_user_id_foreign` (`user_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `status_name_unique` (`name`),
  ADD UNIQUE KEY `status_alias_unique` (`alias`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subscriptions_user_id_stripe_status_index` (`user_id`,`stripe_status`);

--
-- Indexes for table `subscription_items`
--
ALTER TABLE `subscription_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subscription_items_subscription_id_stripe_plan_unique` (`subscription_id`,`stripe_plan`),
  ADD KEY `subscription_items_stripe_id_index` (`stripe_id`);

--
-- Indexes for table `tables`
--
ALTER TABLE `tables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tables_restoarea_id_foreign` (`restoarea_id`),
  ADD KEY `tables_restaurant_id_foreign` (`restaurant_id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `translations_language_id_foreign` (`language_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`),
  ADD UNIQUE KEY `users_verification_code_unique` (`verification_code`),
  ADD KEY `users_stripe_id_index` (`stripe_id`),
  ADD KEY `users_restaurant_id_foreign` (`restaurant_id`);

--
-- Indexes for table `variants`
--
ALTER TABLE `variants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `variants_item_id_foreign` (`item_id`);

--
-- Indexes for table `variants_has_extras`
--
ALTER TABLE `variants_has_extras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `variants_has_extras_variant_id_foreign` (`variant_id`),
  ADD KEY `variants_has_extras_extra_id_foreign` (`extra_id`);

--
-- Indexes for table `visits`
--
ALTER TABLE `visits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visits_restaurant_id_foreign` (`restaurant_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `configs`
--
ALTER TABLE `configs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `expenses_category`
--
ALTER TABLE `expenses_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `expenses_vendor`
--
ALTER TABLE `expenses_vendor`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `extras`
--
ALTER TABLE `extras`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=497;

--
-- AUTO_INCREMENT for table `hours`
--
ALTER TABLE `hours`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=351;

--
-- AUTO_INCREMENT for table `item_has_allergens`
--
ALTER TABLE `item_has_allergens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `localmenus`
--
ALTER TABLE `localmenus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `order_has_items`
--
ALTER TABLE `order_has_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `order_has_status`
--
ALTER TABLE `order_has_status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `paths`
--
ALTER TABLE `paths`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `plan`
--
ALTER TABLE `plan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `restoareas`
--
ALTER TABLE `restoareas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `simple_delivery_areas`
--
ALTER TABLE `simple_delivery_areas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `sms_verifications`
--
ALTER TABLE `sms_verifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscription_items`
--
ALTER TABLE `subscription_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tables`
--
ALTER TABLE `tables`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `variants`
--
ALTER TABLE `variants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `variants_has_extras`
--
ALTER TABLE `variants_has_extras`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visits`
--
ALTER TABLE `visits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `address_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `banners`
--
ALTER TABLE `banners`
  ADD CONSTRAINT `banners_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`),
  ADD CONSTRAINT `banners_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_restorant_id_foreign` FOREIGN KEY (`restorant_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `restorants_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `coupons`
--
ALTER TABLE `coupons`
  ADD CONSTRAINT `coupons_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `expenses`
--
ALTER TABLE `expenses`
  ADD CONSTRAINT `expenses_expenses_category_id_foreign` FOREIGN KEY (`expenses_category_id`) REFERENCES `expenses_category` (`id`),
  ADD CONSTRAINT `expenses_expenses_vendor_id_foreign` FOREIGN KEY (`expenses_vendor_id`) REFERENCES `expenses_vendor` (`id`),
  ADD CONSTRAINT `expenses_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `expenses_category`
--
ALTER TABLE `expenses_category`
  ADD CONSTRAINT `expenses_category_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `expenses_vendor`
--
ALTER TABLE `expenses_vendor`
  ADD CONSTRAINT `expenses_vendor_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `extras`
--
ALTER TABLE `extras`
  ADD CONSTRAINT `extras_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`);

--
-- Constraints for table `hours`
--
ALTER TABLE `hours`
  ADD CONSTRAINT `hours_restorant_id_foreign` FOREIGN KEY (`restorant_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `localmenus`
--
ALTER TABLE `localmenus`
  ADD CONSTRAINT `localmenus_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `options_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_address_id_foreign` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`),
  ADD CONSTRAINT `orders_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `orders_driver_id_foreign` FOREIGN KEY (`driver_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `orders_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `orders_restorant_id_foreign` FOREIGN KEY (`restorant_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `order_has_items`
--
ALTER TABLE `order_has_items`
  ADD CONSTRAINT `order_has_items_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `order_has_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `order_has_status`
--
ALTER TABLE `order_has_status`
  ADD CONSTRAINT `order_has_status_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_has_status_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  ADD CONSTRAINT `order_has_status_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `ratings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `restoareas`
--
ALTER TABLE `restoareas`
  ADD CONSTRAINT `restoareas_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `simple_delivery_areas`
--
ALTER TABLE `simple_delivery_areas`
  ADD CONSTRAINT `simple_delivery_areas_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `sms_verifications`
--
ALTER TABLE `sms_verifications`
  ADD CONSTRAINT `sms_verifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `tables`
--
ALTER TABLE `tables`
  ADD CONSTRAINT `tables_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `companies` (`id`),
  ADD CONSTRAINT `tables_restoarea_id_foreign` FOREIGN KEY (`restoarea_id`) REFERENCES `restoareas` (`id`);

--
-- Constraints for table `translations`
--
ALTER TABLE `translations`
  ADD CONSTRAINT `translations_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `variants`
--
ALTER TABLE `variants`
  ADD CONSTRAINT `variants_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`);

--
-- Constraints for table `variants_has_extras`
--
ALTER TABLE `variants_has_extras`
  ADD CONSTRAINT `variants_has_extras_extra_id_foreign` FOREIGN KEY (`extra_id`) REFERENCES `extras` (`id`),
  ADD CONSTRAINT `variants_has_extras_variant_id_foreign` FOREIGN KEY (`variant_id`) REFERENCES `variants` (`id`);

--
-- Constraints for table `visits`
--
ALTER TABLE `visits`
  ADD CONSTRAINT `visits_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `companies` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
